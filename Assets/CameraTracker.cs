using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTracker : MonoBehaviour
{
	private void Update()
	{
		transform.position = Vector3.Lerp(transform.position, Camera.main.transform.position, Time.deltaTime * 5);
		transform.localRotation = Quaternion.Slerp(transform.localRotation, Camera.main.transform.localRotation, Time.deltaTime * 5);
	}
}
