//the application data
struct appdata {
    float4 vertex : POSITION;
    float2 uv : TEXCOORD0;
    float2 uv1 : TEXCOORD1;
    float2 uv2 : TEXCOORD2;
    float4 color : COLOR;  // set from Image component property
};

// from vertext to fragment data
struct v2f {
    float2 uv : TEXCOORD0;
    float2 uv1 : TEXCOORD1;
    float2 uv2 : TEXCOORD2;
    float4 worldPosition : TEXCOORD3;
    float4 vertex : SV_POSITION;
    float4 color : COLOR;
};

#include "UnityUI.cginc"
float4 _ClipRect;
sampler2D _MaskTex;

// the vertex part
v2f vert(appdata v) {
    v2f o;
    o.vertex = UnityObjectToClipPos(v.vertex);
    o.worldPosition = v.vertex;
    o.uv = v.uv;
    o.uv1 = v.uv1;
    o.uv2 = v.uv2;
    o.color = v.color;
    return o;
}

// mixes the alpha
inline fixed4 mixAlpha(fixed4 mainTexColor,float2 uv, fixed4 color,float4 world, float sdfAlpha) {
    fixed4 col = mainTexColor * color;
    col.a = min(col.a, sdfAlpha);
    col.a *= tex2D(_MaskTex, uv).r;
    col.a *= UnityGet2DClipping(world.xy, _ClipRect);

#ifdef UNITY_UI_ALPHACLIP
    clip(color.a - 0.001);
#endif

    return col;
}