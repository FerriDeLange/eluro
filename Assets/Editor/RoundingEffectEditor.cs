using UnityEngine;
using UnityEditor;
using Eluro;
using UnityEngine.UI;
using System.Linq;
using System.IO;
using System;
using System.Collections.Generic;

[CustomEditor(typeof(RoundingEffect))]
[CanEditMultipleObjects]
public class RoundingEffectEditor : Editor
{
    #region Fields
    #region Serialized Properties
    /// <summary>
    /// The property for automatically rounding the sprite
    /// </summary>
    private SerializedProperty autoRound;

    /// <summary>
    /// The property for the amount of rounding applied
    /// </summary>
    private SerializedProperty amount;

    /// <summary>
    /// The property for the amount of border applied
    /// </summary>
    private SerializedProperty amountBorder;
    #endregion
    #region Private Fields
    /// <summary>
    /// A reference to the <see cref="RoundingEffect"/> this custom inspector is attached to
    /// </summary>
    private RoundingEffect roundingEffect;

    /// <summary>
    /// Reference to the canvas the image component resides in
    /// </summary>
    private Canvas canvas;

    /// <summary>
    /// Holds all the errors
    /// </summary>
    private Dictionary<Errors, Error> errors;
    #endregion
    #region Properties
    /// <summary>
    /// Whether we have at least one error active
    /// </summary>
    public bool AnyErrors => errors.Any((KeyValuePair<Errors, Error> error) => error.Value.value);
    #endregion
    #region Paths
    /// <summary>
    /// Path for the directory of the materials and shaders
    /// </summary>
    private static string directoryPath;

    /// <summary>
    /// Path for the standard filled UI round
    /// </summary>
    private static string fillShaderPath;

    /// <summary>
    /// Path for the rounding shader with borders
    /// </summary>
    private static string borderShaderPath;

    /// <summary>
    /// Project relative path for the standard filled UI round
    /// </summary>
    private static string relativeFillShaderPath;

    /// <summary>
    /// Project relative path for the rounding shader with borders
    /// </summary>
    private static string relativeBorderShaderPath;

    /// <summary>
    /// Path to the fill material
    /// </summary>
    private static string fillMaterialPath;

    /// <summary>
    /// Path to the border material
    /// </summary>
    private static string borderMaterialPath;
    #endregion
    #endregion
    #region Methods
    #region Unity
    /// <summary>
    /// Get starting values
    /// </summary>
    void OnEnable()
    {
        roundingEffect = target as RoundingEffect;
        canvas = roundingEffect.GetComponentInParent<Canvas>();

        FindProperties(serializedObject, out amount, out amountBorder, out autoRound);
        InitializePaths();
        errors = InitializeErrors();
        AddFixMethods(ref errors);
    }

    /// <summary>
    /// Update loop for the inspector
    /// </summary>
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        if (AnyErrors)
            EditorGUILayout.HelpBox("In RoundingEffect.cs you can change the path values to reflect your own project needs, do contact a project manager for this.", MessageType.Info);
        EditorGUI.BeginDisabledGroup(AnyErrors);
        LayoutHeaderButtons();
        LayoutProperties();
        EditorGUI.EndDisabledGroup();

        CheckForErrors(ref errors, canvas);
        if (AnyErrors)
            ErrorHandling(errors);

        serializedObject.ApplyModifiedProperties();
    }
    #endregion
    #region Layout
    /// <summary>
    /// Lays out the top buttons where you can select your rounding mode
    /// </summary>
    private void LayoutHeaderButtons()
    {
        GUIStyle active = new GUIStyle(GUI.skin.button);
        active.fontStyle = FontStyle.Bold;
        active.fontSize = 13;

        GUIStyle fill = new GUIStyle(roundingEffect.Mode == RoundingEffect.RoundingMode.Fill ? active : GUI.skin.button);
        GUIStyle border = new GUIStyle(roundingEffect.Mode == RoundingEffect.RoundingMode.Border ? active : GUI.skin.button);

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Fill", fill, null))
        {
            roundingEffect.Mode = RoundingEffect.RoundingMode.Fill;
            EditorUtility.SetDirty(roundingEffect);
        }
        if (GUILayout.Button("Border", border, null))
        {
            roundingEffect.Mode = RoundingEffect.RoundingMode.Border;
            EditorUtility.SetDirty(roundingEffect);
        }
        EditorGUILayout.EndHorizontal();
    }

    /// <summary>
    /// Lays out the remaining properties
    /// </summary>
    private void LayoutProperties()
    {
        EditorGUILayout.PropertyField(autoRound);

        if (!roundingEffect.AutoRounding)
            EditorGUILayout.PropertyField(amount);

        if (roundingEffect.Mode == RoundingEffect.RoundingMode.Border)
            EditorGUILayout.PropertyField(amountBorder);
    } 
    #endregion
    #region Initialization
    /// <summary>
    /// Initializes the error dictionary
    /// </summary>
    /// <returns>The initialized error dictionary</returns>
    private static Dictionary<Errors, Error> InitializeErrors()
        => new Dictionary<Errors, Error>()
        {
            { Errors.CanvasMissing,              new Error(false, Errors.CanvasMissing, null) },
            { Errors.TexCoord1Missing,           new Error(false, Errors.TexCoord1Missing, null) },
            { Errors.TexCoord2Missing,           new Error(false, Errors.TexCoord2Missing, null) },
            { Errors.DirectoryPathNotFound,      new Error(false, Errors.DirectoryPathNotFound, null) },
            { Errors.FillMaterialPathNotFound,   new Error(false, Errors.FillMaterialPathNotFound, null) },
            { Errors.BorderMaterialPathNotFound, new Error(false, Errors.BorderMaterialPathNotFound, null) },
            { Errors.FillShaderPathNotFound,     new Error(false, Errors.FillShaderPathNotFound, null) },
            { Errors.BorderShaderPathNotFound,   new Error(false, Errors.BorderShaderPathNotFound, null) },
            { Errors.FillMaterialWrongShader,    new Error(false, Errors.FillMaterialWrongShader, null) },
            { Errors.BorderMaterialWrongShader,  new Error(false, Errors.BorderMaterialWrongShader, null) }
        };

    /// <summary>
    /// Adds all the fixing methods to the errors
    /// </summary>
    /// <param name="errors"></param>
    private void AddFixMethods(ref Dictionary<Errors, Error> errors)
    {
        errors[Errors.TexCoord1Missing].Fix += () => canvas.additionalShaderChannels |= AdditionalCanvasShaderChannels.TexCoord1;
        errors[Errors.TexCoord2Missing].Fix += () => canvas.additionalShaderChannels |= AdditionalCanvasShaderChannels.TexCoord2;
        errors[Errors.FillMaterialWrongShader].Fix += () => RoundingEffect.FillMaterial.shader = AssetDatabase.LoadAssetAtPath(relativeFillShaderPath, typeof(Shader)) as Shader;
        errors[Errors.BorderMaterialWrongShader].Fix += () => RoundingEffect.BorderMaterial.shader = AssetDatabase.LoadAssetAtPath(relativeBorderShaderPath, typeof(Shader)) as Shader;
    }

    /// <summary>
    /// Initializes all the path values
    /// </summary>
    private static void InitializePaths()
    {
        directoryPath = $"{Application.dataPath}/{RoundingEffect.RELATIVE_DIRECTORY}";
        fillShaderPath = $"{directoryPath}/{RoundingEffect.FILL_SHADER_FILE_NAME}";
        borderShaderPath = $"{directoryPath}/{RoundingEffect.BORDER_SHADER_FILE_NAME}";
        relativeFillShaderPath = $"Assets/{RoundingEffect.RELATIVE_DIRECTORY}/{RoundingEffect.FILL_SHADER_FILE_NAME}";
        relativeBorderShaderPath = $"Assets/{RoundingEffect.RELATIVE_DIRECTORY}/{RoundingEffect.BORDER_SHADER_FILE_NAME}";
        fillMaterialPath = $"{directoryPath}/{RoundingEffect.FILL_MATERIAL_FILE_NAME}";
        borderMaterialPath = $"{directoryPath}/{RoundingEffect.BORDER_MATERIAL_FILE_NAME}";
    }

    /// <summary>
    /// Finds all the serialized properties
    /// </summary>
    /// <param name="serializedObject">The target object</param>
    /// <param name="amount">Outputs the amount</param>
    /// <param name="amountBorder">Outputs the border amount</param>
    /// <param name="autoRound">Outputs the auto round</param>
    private static void FindProperties(in SerializedObject serializedObject, out SerializedProperty amount, out SerializedProperty amountBorder, out SerializedProperty autoRound)
    {
        amount = serializedObject.FindProperty("amount");
        amountBorder = serializedObject.FindProperty("borderAmount");
        autoRound = serializedObject.FindProperty("autoRounding");
    }
    #endregion
    #region Error Handling
    /// <summary>
    /// Checks if we have any errors
    /// </summary>
    /// <param name="errors">Returns all the errors</param>
    /// <param name="canvas">Needed to check whether it exists and if it has the right additional shader channels</param>
    private static void CheckForErrors(ref Dictionary<Errors, Error> errors, in Canvas canvas)
    {
        errors[Errors.CanvasMissing].value = canvas == null;
        errors[Errors.TexCoord1Missing].value = errors[Errors.CanvasMissing].value ? true : !canvas.additionalShaderChannels.HasFlag(AdditionalCanvasShaderChannels.TexCoord1);
        errors[Errors.TexCoord2Missing].value = errors[Errors.CanvasMissing].value ? true : !canvas.additionalShaderChannels.HasFlag(AdditionalCanvasShaderChannels.TexCoord2);
        errors[Errors.DirectoryPathNotFound].value = !Directory.Exists(directoryPath);
        errors[Errors.FillShaderPathNotFound].value = !File.Exists(fillShaderPath);
        errors[Errors.BorderShaderPathNotFound].value = !File.Exists(borderShaderPath);
        errors[Errors.FillMaterialPathNotFound].value = !File.Exists(fillMaterialPath);
        errors[Errors.BorderMaterialPathNotFound].value = !File.Exists(borderMaterialPath);
        errors[Errors.FillMaterialWrongShader].value = AssetDatabase.GetAssetPath(RoundingEffect.FillMaterial?.shader) != relativeFillShaderPath;
        errors[Errors.BorderMaterialWrongShader].value = AssetDatabase.GetAssetPath(RoundingEffect.BorderMaterial?.shader) != relativeBorderShaderPath;
    }

    /// <summary>
    /// Does the error handling by showing all the help boxes and fix buttons
    /// </summary>
    /// <param name="errors">The actual errors used to show information about</param>
    private static void ErrorHandling(Dictionary<Errors, Error> errors)
    {
        if (errors[Errors.CanvasMissing].value)
            EditorGUILayout.HelpBox("Make sure there is a canvas", MessageType.Warning);
        else if (errors[Errors.TexCoord1Missing].value)
        {
            EditorGUILayout.HelpBox("TexCoord1 in your canvas's Additional Shader Channels is turned off", MessageType.Warning);
            if (GUILayout.Button("Fix"))
                errors[Errors.TexCoord1Missing].Fix?.Invoke();
        }
        else if (errors[Errors.TexCoord2Missing].value)
        {
            EditorGUILayout.HelpBox("TexCoord2 in your canvas's Additional Shader Channels is turned off", MessageType.Warning);
            if (GUILayout.Button("Fix"))
                errors[Errors.TexCoord2Missing].Fix?.Invoke();
        }
        else if (errors[Errors.DirectoryPathNotFound].value)
            EditorGUILayout.HelpBox($"Target directory for materials can't be found: {directoryPath}", MessageType.Warning);
        else if (errors[Errors.FillShaderPathNotFound].value)
            EditorGUILayout.HelpBox($"Fill shader can't be found.\nExpected path: {fillShaderPath}", MessageType.Warning);
        else if (errors[Errors.BorderShaderPathNotFound].value)
            EditorGUILayout.HelpBox($"Border shader can't be found.\nExpected path: {borderShaderPath}", MessageType.Warning);
        else if (errors[Errors.FillMaterialPathNotFound].value)
            EditorGUILayout.HelpBox($"Fill material can't be found.\nPath: {fillMaterialPath}", MessageType.Warning);
        else if (errors[Errors.BorderMaterialPathNotFound].value)
            EditorGUILayout.HelpBox($"Border material can't be found.\nPath: {borderMaterialPath}", MessageType.Warning);
        else if (errors[Errors.FillMaterialWrongShader].value)
        {
            bool missingShader = RoundingEffect.FillMaterial != null || string.IsNullOrEmpty(RoundingEffect.FillMaterial.shader.name);
            string msg = $"Fill material has the wrong shader applied." +
                $"{(missingShader ? "\nCurrent: " : "")}{RoundingEffect.FillMaterial?.shader.name}" +
                $"{(missingShader ? "\nExpected: " : "")}{relativeFillShaderPath}";
            EditorGUILayout.HelpBox(msg, MessageType.Warning);

            if (GUILayout.Button("Fix"))
                errors[Errors.FillMaterialWrongShader].Fix?.Invoke();
        }
        else if (errors[Errors.BorderMaterialWrongShader].value)
        {
            bool missingShader = RoundingEffect.BorderMaterial != null || string.IsNullOrEmpty(RoundingEffect.BorderMaterial.shader.name);
            string msg = $"Border material has the wrong shader applied." +
                $"{(missingShader ? "\nCurrent: " : "")}{RoundingEffect.BorderMaterial?.shader.name}" +
                $"{(missingShader ? "\nExpected: " : "")}{relativeBorderShaderPath}";
            EditorGUILayout.HelpBox(msg, MessageType.Warning);

            if (GUILayout.Button("Fix"))
                errors[Errors.BorderMaterialWrongShader].Fix?.Invoke();
        }
    }
    #endregion
    #endregion
    #region Data
    /// <summary>
    /// Defines all the error types
    /// </summary>
    public enum Errors
    {
        TexCoord1Missing,
        TexCoord2Missing,
        CanvasMissing,
        DirectoryPathNotFound,
        FillMaterialPathNotFound,
        BorderMaterialPathNotFound,
        FillShaderPathNotFound,
        BorderShaderPathNotFound,
        FillMaterialWrongShader,
        BorderMaterialWrongShader,
    }

    /// <summary>
    /// Holds data about errors and if possible a fix method
    /// </summary>
    internal class Error
    {
        public bool value;
        public Errors type;
        public Action Fix;
        public Error(bool value, Errors type, Action Fix)
        {
            this.value = value;
            this.type = type;
            this.Fix = Fix;
        }
    } 
    #endregion
}