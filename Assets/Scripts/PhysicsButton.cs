using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Eluro.Eluro
{
	public class PhysicsButton : MonoBehaviour
	{
		#region Members
		[SerializeField]
		private float threshold = 0.1f;

		[SerializeField]
		private float deadZone = 0.025f;

		private bool isPressed;
		private Vector3 startPos;
		private ConfigurableJoint joint;

		public event Action OnPressed, OnReleased;
		#endregion
		#region Unity Methods
		public void Start()
		{
			startPos = transform.localPosition;
			joint = GetComponent<ConfigurableJoint>();
		}

		private void Update()
		{
			if(!isPressed && GetValue() + threshold >= .4f)
				Pressed();
			if (isPressed && GetValue() - threshold <= 0)
				Released();
		}
		#endregion
		#region Methods
		private float GetValue()
		{
			float value = Vector3.Distance(startPos, transform.localPosition) / joint.linearLimit.limit;

			if(Mathf.Abs(value) < deadZone)
			{
				value = 0;
			}

			return Mathf.Clamp(value, -1, 1);
		}

		private void Pressed()
		{
			isPressed = true;
			OnPressed?.Invoke();
		}

		private void Released()
		{
			isPressed = false;
			OnReleased?.Invoke();
		}
		#endregion
	}
}