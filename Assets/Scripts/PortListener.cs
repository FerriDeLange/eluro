using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Eluro.Eluro
{
    public class PortListener : MonoBehaviour
    {
		public Vector2 Input { get; private set; }

		private string queue = "";

		private void OnSerialData(string msg)
		{
			queue += msg;
			int index = queue.LastIndexOf('\n');
			if (index == -1)
				return;

			string[] prevValues = queue.Substring(0, index).Split('\n');
			List<Vector2> input = new List<Vector2>();
			for (int i = 0; i < prevValues.Length; i++)
			{
				string[] vector = prevValues[i].Split(',');
				if(float.TryParse(vector[0], out float x) && float.TryParse(vector[1], out float y))
					input.Add(new Vector2((x / 1024.0f - 0.5f) * 2, (y / 1024.0f - 0.5f) * 2));
			}

			Input = input.Last();

			queue = queue.Remove(0, index + 1);
		}
	}
}