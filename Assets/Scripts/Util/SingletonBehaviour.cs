using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Eluro.Utility
{
	/// <summary>
	/// Inherit from this class to make any monobehaviour a Singleton.
	/// </summary>
	/// <typeparam name="T">Will create a singleton monobehaviour.</typeparam>
	public abstract class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
	{
		#region Variables
		#region Public
		/// <summary>
		/// Whether an instance exists.
		/// </summary>
		public static bool Exists => instance != null;

		/// <summary>
		/// Singleton-Reference.
		/// Auto-Creates GameObject if it does not exist.
		/// </summary>
		public static T Instance
		{
			get
			{
				if (instance != null)
					return instance;

				GameObject obj = new GameObject($"{typeof(T).Name}-Singleton");
				return obj.AddComponent<T>();
			}
			protected set => instance = value;
		}
		#endregion

		#region Protected
		/// <summary>
		/// Internal Singleton-Reference.
		/// Protected get so it can be overridden through inheritance.
		/// </summary>
		protected static T instance { get; private set; }

		/// <summary>
		/// Whether this Singleton has a Root-Object. If true, root-Object will be added to DontDestroyOnLoad instead
		/// </summary>
		[SerializeField]
		[Tooltip("Whether this Singleton has a Root-Object. If true, root-Object will be added to DontDestroyOnLoad instead")]
		protected bool hasRootObject;
		#endregion

		#region Methods
		/// <summary>
		/// Singleton-Setup
		/// </summary>
		protected virtual void Awake()
		{
			if (instance != null && instance != this)
			{
				Debug.LogWarning($"Singleton<{typeof(T).Name}> already exists! Existing Object: {instance.gameObject.name}. Destroying new object {gameObject.name}", gameObject);
				Destroy(gameObject);
				return;
			}
			if (!instance && transform.parent != null)
				Debug.LogWarning($"Singleton<{typeof(T).Name}> on {gameObject.name} is not a root-object. Did you mean to set HasRootObject?");
			if (!hasRootObject)
				DontDestroyOnLoad(gameObject);
			else
				DontDestroyOnLoad(transform.root.gameObject);
			instance = this as T;
		}

		/// <summary>
		/// Singleton-Destruction
		/// </summary>
		protected virtual void OnDestroy()
		{
			if (Exists && instance == this)
				instance = null;
		}
		#endregion
		#endregion
	}
}