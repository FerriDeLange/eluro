#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

namespace Eluro.Util
{
	internal sealed class ScriptKeywordProcessor : UnityEditor.AssetModificationProcessor
	{
		/// <summary>
		/// All the different characters we want to split namespace strings at.
		/// </summary>
		private static char[] _spliters = new char[] { '/', '\\', '.' };

		/// <summary>
		/// The words to delete from our relative path that will change into the namespace.
		/// </summary>
		private static List<string> _wordsToDelete = new List<string> { "Extensions", "Scripts", "Editor" };

		/// <summary>
		/// Gets called when the asset will be created. 
		/// Here we update the namespace to reflect our folder path
		/// </summary>
		/// <param name="path"></param>
		public static void OnWillCreateAsset(string path)
		{
			path = path.Replace(".meta", "");
			int index = path.LastIndexOf(".");
			if (index < 0)
				return;

			string file = path.Substring(index);
			if (file != ".cs" && file != ".js")
				return;

			List<string> namespaces = path.Split(_spliters).ToList();
			namespaces = namespaces.GetRange(1, namespaces.Count - 3);
			namespaces = namespaces.Except(_wordsToDelete).ToList();

			string namespaceString = "Eluro";
			for (int i = 0; i < namespaces.Count; i++)
			{
				if (i == 0)
					namespaceString = "";
				namespaceString += namespaces[i];
				if (i < namespaces.Count - 1)
					namespaceString += ".";
			}

			index = Application.dataPath.LastIndexOf("Assets");
			path = Application.dataPath.Substring(0, index) + path;
			if (!System.IO.File.Exists(path))
				return;

			string fileContent = System.IO.File.ReadAllText(path);
			namespaceString = namespaceString.Insert(0, "Eluro.");
			fileContent = fileContent.Replace("#NAMESPACE#", namespaceString);
			System.IO.File.WriteAllText(path, fileContent);
			AssetDatabase.Refresh();
		}
	}
}
#endif