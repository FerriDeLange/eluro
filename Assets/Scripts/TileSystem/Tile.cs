using Eluro.Tiles;
using Eluro.WaveSystem;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class Tile : MonoBehaviour
{

	/// <summary>
	/// The points to check on what other box they can connect.
	/// </summary>
	[SerializeField]
	private int entryNumber;

	[SerializeField]
    private int exitNumber;

	[SerializeField]
	private List<SpawnRegion> spawnRegions;

	public ReadOnlyCollection<SpawnRegion> SpawnRegions => spawnRegions.AsReadOnly();

	public int EntryNumer => entryNumber;
	public int ExitNumber => exitNumber;

    /// <summary>
    /// Draws the box to make an example on how it will look.
    /// </summary>
    public void OnDrawGizmos()
    {
        // OnDrawGizmos(float boxLength) can't take a parameter, thats why its not working. tilemanager.tileSize is a const and i can't get that value. //
        var pos = transform.position;
        var endPos = IslandPlacer.TILE_SIZE / 2;

        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, new Vector3(IslandPlacer.TILE_SIZE, 0, IslandPlacer.TILE_SIZE));
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(new Vector3(pos.x - endPos, pos.y, pos.z), new Vector3(pos.x + endPos, pos.y, pos.z));
        Gizmos.DrawLine(new Vector3(pos.x, pos.y, pos.z - endPos), new Vector3(pos.x, pos.y, pos.z + endPos));

#if UNITY_EDITOR
		GUIStyle style = new GUIStyle(GUI.skin.label);
		style.fontSize = 40;
		style.alignment = TextAnchor.MiddleCenter;
		Handles.Label(transform.position - Vector3.right * IslandPlacer.TILE_SIZE / 2 + Vector3.up, entryNumber.ToString(), style);
		Handles.Label(transform.position + Vector3.right * IslandPlacer.TILE_SIZE / 2 + Vector3.up, exitNumber.ToString(), style);
#endif
	}
}
