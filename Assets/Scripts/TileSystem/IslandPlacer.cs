using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Eluro.Tiles
{
	/// <summary>
	/// Places the islands.
	/// </summary>
	public class IslandPlacer : TilePlacer<Tile>
	{
		/// <summary>
		/// Query that returns the next collection of islands that are available.
		/// </summary>
		private IEnumerable<Tile> forwardMatchingTilesQuery;

		/// <summary>
		/// The radius to check for placing new islands.
		/// </summary>
		public override float CheckRadius => 300;

		/// <summary>
		/// The size of the islands.
		/// </summary>
		public override float Size => TILE_SIZE;

		/// <summary>
		/// The size of the islands.
		/// </summary>
		public const float TILE_SIZE = 75;

		/// <summary>
		/// Define behaviours.
		/// </summary>
		private void Awake()
		{
			forwardMatchingTilesQuery = from tile in tilePrefabs
										where activeTiles.Count == 0 || tile.EntryNumer == activeTiles.Last().ExitNumber
										select tile;
		}

		/// <summary>
		/// Checks whether islands can be placed.
		/// </summary>
		private void Update() => CheckForPlacement();

		/// <summary>
		/// Returns a random tile from the <see cref="forwardMatchingTilesQuery"/>.
		/// </summary>
		/// <returns>A random tile that can be used.</returns>
		protected override Tile GetTile()
		{
			var tiles = forwardMatchingTilesQuery.ToArray();
			return tiles[Random.Range(0, tiles.Length)];
		}
	} 
}
