using System;
using System.Collections.Generic;
using UnityEngine;

namespace Eluro.Tiles
{
	/// <summary>
	/// Generic behaviour for placing prefabs in a direction with clean up.
	/// </summary>
	/// <typeparam name="T">The component to instantiate the gameobject with.</typeparam>
	public abstract class TilePlacer<T> : MonoBehaviour where T : Component
	{
		/// <summary>
		/// The target to follow placement.
		/// </summary>
		[SerializeField]
		protected Transform target;

		/// <summary>
		/// The prefabs that are avaiable for use.
		/// </summary>
		[SerializeField]
		protected T[] tilePrefabs;

		/// <summary>
		/// The tiles that are currently active.
		/// </summary>
		protected Queue<T> activeTiles = new Queue<T>();

		/// <summary>
		/// The size of the tile.
		/// </summary>
		public abstract float Size { get; }

		/// <summary>
		/// The radius to check for in.
		/// </summary>
		public abstract float CheckRadius { get; }

		/// <summary>
		/// The counter of the amount of objects placed.
		/// </summary>
		private int counter;

		/// <summary>
		/// Called when a tile is a spawned.
		/// </summary>
		public event Action<T> TileSpawned;

		/// <summary>
		/// Will be called just before the tile is destroyed.
		/// </summary>
		public event Action<T> TileDestroyed;

		/// <summary>
		/// Checks whether new objects need to be placed or destroyed and if so does so.
		/// </summary>
		protected void CheckForPlacement()
		{
			float distance;
			do
			{
				distance = Vector3.Distance(Vector3.right * Size * (counter + 1), target.localPosition);
				if (distance < CheckRadius)
				{
					PlaceTileForward();
				}
			} while (distance < CheckRadius);

			if (activeTiles.Count == 0)
				return;

			do
			{
				distance = Vector3.Distance(activeTiles.Peek().transform.localPosition, target.localPosition);
				if (distance > CheckRadius)
				{
					T tile = activeTiles.Dequeue();
					TileDestroyed?.Invoke(tile);
					Destroy(tile.gameObject);
				}
			} while (distance > CheckRadius);
		}

		/// <summary>
		/// Places a new tile.
		/// </summary>
		private void PlaceTileForward()
		{
			T newTile = Instantiate(GetTile());
			activeTiles.Enqueue(newTile);
			newTile.transform.SetParent(transform);
			newTile.transform.SetAsLastSibling();
			newTile.transform.localPosition = Vector3.right * Size * counter;
			++counter;
			TileSpawned?.Invoke(newTile);
		}

		/// <summary>
		/// Returns a tile that can be used for instantation.
		/// </summary>
		/// <returns>A tile that can be used for instantation.</returns>
		protected abstract T GetTile();
	}
}