using System.Linq;
using UnityEngine;

namespace Eluro.Tiles
{
	/// <summary>
	/// Places ocean tiles.
	/// </summary>
	public class OceanPlacer : TilePlacer<MeshRenderer>
	{
		/// <summary>
		/// The size of the ocean tiles.
		/// </summary>
		public override float Size => 50;

		/// <summary>
		/// The radius to check for placing and destroying tiles.
		/// </summary>
		public override float CheckRadius => 600;

		/// <summary>
		/// Checks whether a new tile can be placed or cleaned up.
		/// </summary>
		private void Update() => CheckForPlacement();

		/// <summary>
		/// Returns the ocean tile.
		/// </summary>
		/// <returns>A new ocean tile.</returns>
		protected override MeshRenderer GetTile() => tilePrefabs.First();
	} 
}