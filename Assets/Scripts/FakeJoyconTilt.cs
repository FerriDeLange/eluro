using Eluro.InputManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Eluro.Eluro
{
    public class FakeJoyconTilt : MonoBehaviour
    {
		#region Members
        #endregion
        #region Unity Methods
        public void Update()
        {
			Vector3 raw = new Vector3(InputManager.Instance.Horizontal * 20, 0, InputManager.Instance.Vertical * 20);
			transform.localEulerAngles = raw;
		}
        #endregion
        #region Methods
        #endregion
    }
}