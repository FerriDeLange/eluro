using UnityEngine;

namespace Eluro.ProceduralGeneration
{
	/// <summary>
	/// This class generates a mesh and positions it on a given transform upon generation.
	/// Requires a MeshFilter and a MeshRenderer component.
	/// </summary>
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
	[ExecuteInEditMode]
    public class MeshGenerator : MonoBehaviour
    {
		#region Members
		/// <summary>
		/// The size of the mesh on the x-axis.
		/// </summary>
		private int xSize = 25;

		/// <summary>
		/// The size of the mesh on the y-axis.
		/// </summary>
		private int zSize = 25;

		/// <summary>
		/// Amount of polygons per unit of size.
		/// </summary>
		private int polygonMultiplier = 12;

		/// <summary>
		/// A reference to the mesh we will create.
		/// </summary>
        private Mesh mesh;
        #endregion
        #region Unity Methods

		/// <summary>
		/// This method is called to generate the mesh and position it.
		/// </summary>
        public void Awake()
        {
            GenerateMesh();
        }

        #endregion
        #region Methods
		private void GenerateMesh()
		{
			// Calculate the size of the mesh.
			xSize *= polygonMultiplier;
			zSize *= polygonMultiplier;

			// Create the base mesh and assign it to the filter.
			GetComponent<MeshFilter>().mesh = mesh = new Mesh();
			mesh.name = "Procedural Water";

			// Create an array of positions vertices.
			Vector3[] vertices = new Vector3[(xSize + 1) * (zSize + 1)];

			// Create an array of uvs.
			Vector2[] uv = new Vector2[vertices.Length];

			// Two dimensional loop to create positions for each vertex and uv.
			for (int i = 0, z = 1; z <= zSize + 1; z++)
			{
				for (int x = 1; x <= xSize + 1; x++, i++)
				{
					// Divide vector3 for vertex by polygonMultiplier to create smaller vertices.
					vertices[i] = new Vector3(x, 0, z) / polygonMultiplier;

					// Multiply vector2 by polygonMultiplier, to prevent using a high noise value for water shader.
					uv[i] = new Vector2((float)x / xSize, (float)z / zSize) * polygonMultiplier;
				}
			}

			// Create an int array to hold triangles.
			int[] triangles = new int[xSize * zSize * 6];

			// Each vertex holds 6 triangle coordinates.
			for (int ti = 0, vi = 0, z = 0; z < zSize; z++, vi++)
			{
				for (int x = 0; x < xSize; x++, ti += 6, vi++)
				{
					// Assign triangles.
					triangles[ti] = vi;
					triangles[ti + 3] = triangles[ti + 2] = vi + 1;
					triangles[ti + 4] = triangles[ti + 1] = vi + xSize + 1;
					triangles[ti + 5] = vi + xSize + 2;
				}
			}

			// Update the mesh with new values.
			UpdateMesh(vertices, uv, triangles);
		}

		/// <summary>
		/// This method clears the mesh of previous (if any) values, and sets new values.
		/// </summary>
		/// <param name="vertices">The vertices of the mesh.</param>
		/// <param name="uv">The uv of the mesh.</param>
		/// <param name="triangles">The triangles of the mesh.</param>
		private void UpdateMesh(Vector3[] vertices, Vector2[] uv, int[] triangles)
        {
			mesh.Clear();
			mesh.vertices = vertices;
			mesh.uv = uv;
			mesh.triangles = triangles;
			mesh.RecalculateNormals();
		}
		#endregion
	}
}