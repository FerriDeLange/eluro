using Eluro.InputManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class Hand : MonoBehaviour
{
    [SerializeField]
    private SteamVR_RenderModel controllerModel;

    private Interactible m_curentInteractible = null;
    public List<Interactible> m_contactInteracitbles = new List<Interactible>();

    public bool GrabbedJoycon { get; private set; }

    private float? previousGrab = null;

    private void Awake()
    {
        controllerModel = GetComponentInChildren<SteamVR_RenderModel>();
    }

    private void Update()
    {

        //Get trigger down input.
        if (InputManager.Instance.Grab > 0 && previousGrab == 0 && previousGrab != null)
        {
            Pickup();
        }
        else if (InputManager.Instance.Grab < 0.5 && m_curentInteractible?.m_activeHand)
        {
            m_curentInteractible.m_activeHand.Drop();
        }

        previousGrab = InputManager.Instance.Grab;
    }

    private void OnTriggerEnter(Collider other)
    {
        //Tag check.
        if (!other.gameObject.CompareTag("JoyStick"))
            return;
        
        m_contactInteracitbles.Add(other.gameObject.GetComponent<Interactible>());
    }

    private void OnTriggerExit(Collider other)
    {
        //Tag check.
        if (!other.gameObject.CompareTag("JoyStick"))
            return;
        
        m_curentInteractible?.m_activeHand.Drop();
        m_contactInteracitbles.Remove(other.gameObject.GetComponent<Interactible>());
    }

    public void Pickup()
    {
        //Get nearest.
        m_curentInteractible = GetNearestInteractible();
        
        //Null check.
        if (!m_curentInteractible)
            return;

        //Set active hand.
        m_curentInteractible.m_activeHand = this;
        controllerModel.gameObject.SetActive(false);

        GrabbedJoycon = true;
    }

    public void Drop()
    {
        //Null check.
        if (!m_curentInteractible)
            return;
        
        //Clear
        m_curentInteractible.m_activeHand = null;
        m_curentInteractible = null;
        controllerModel.gameObject.SetActive(true);
        GrabbedJoycon = false;

    }

    private Interactible GetNearestInteractible()
    {
        Interactible nearest = null;
        float minDistance = float.MaxValue;
        float distance = 0.0f;
        foreach (Interactible interactible in m_contactInteracitbles)
        {
            distance = (interactible.transform.position - transform.position).sqrMagnitude;

            if (distance < minDistance)
            {
                minDistance = distance;
                nearest = interactible;
            }
        }
        return nearest;
    }
}