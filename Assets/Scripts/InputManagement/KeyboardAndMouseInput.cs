using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Eluro.InputManagement
{
	/// <summary>
	/// The input manager for keyboard and mouse controls.
	/// </summary>
    public class KeyboardAndMouseInput : IInput
	{
		#region Members
		private Mouse mouse
		{
			get
			{
				if (Mouse.current == null)
					throw new InputDeviceNotFoundException(typeof(Mouse), "The mouse can't be found.");
				return Mouse.current;
			}
		}

		private Keyboard keyboard
		{
			get
			{
				if (Keyboard.current == null)
					throw new InputDeviceNotFoundException(typeof(Keyboard), "The keyboard can't be found.");
				return Keyboard.current;
			}
		}

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public float Horizontal => Mathf.Clamp(mouse.delta.x.ReadValue(), -10, 10) / 10;

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public float Vertical => Mathf.Clamp(mouse.delta.y.ReadValue(), -10, 10) / 10;

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public float Trigger => mouse.leftButton.ReadValue();

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public float Grab => 0;

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public bool Button1 => keyboard.zKey.ReadValue() > Mathf.Epsilon;

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public bool Button2 => keyboard.xKey.ReadValue() > Mathf.Epsilon;
		#endregion
	}
}