using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Valve.VR;

namespace Eluro.InputManagement
{
    /// <summary>
    /// The input manager for a standard game controller.
    /// </summary>
    public class VRInput : IInput
    {
        private SteamVR_ActionSet primarySet;

        private SteamVR_Action_Single m_PinchVector;
        private SteamVR_Action_Single m_GrabVector;
        private SteamVR_Action_Vector2 m_TrackVector;

        private SteamVR_Action_Boolean m_Button1;
        private SteamVR_Action_Boolean m_Button2;

        private SteamVR_Behaviour_Pose m_Pose = null;

        private Hand rightHand, leftHand;
        #region Members

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public float Horizontal => rightHand.GrabbedJoycon || leftHand.GrabbedJoycon ? m_TrackVector.GetAxis(m_Pose.inputSource).x : 0;

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public float Vertical => rightHand.GrabbedJoycon || leftHand.GrabbedJoycon ? m_TrackVector.GetAxis(m_Pose.inputSource).y : 0;

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public float Trigger => rightHand.GrabbedJoycon || leftHand.GrabbedJoycon ? m_PinchVector.GetAxis(m_Pose.inputSource) : 0;

        /// <summary>
        /// <inherithdoc/>
        /// </summary>
        public float Grab => m_GrabVector.GetAxis(m_Pose.inputSource);

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public bool Button1 => m_Button1.GetStateDown(m_Pose.inputSource);

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public bool Button2 => m_Button2.GetStateDown(m_Pose.inputSource);
        #endregion
        #region Constructor
        public VRInput(
            SteamVR_ActionSet primarySet, 
            SteamVR_Action_Single m_PinchVector, 
            SteamVR_Action_Single m_GrabVector, 
            SteamVR_Action_Vector2 m_TrackVector, 
            SteamVR_Action_Boolean m_Button1, 
            SteamVR_Action_Boolean m_Button2, 
            SteamVR_Behaviour_Pose m_Pose,
            Hand rightHand,
            Hand leftHand
           )
        {
            this.primarySet = primarySet;
            this.m_PinchVector = m_PinchVector;
            this.m_GrabVector = m_GrabVector;
            this.m_TrackVector = m_TrackVector;
            this.m_Button1 = m_Button1;
            this.m_Button2 = m_Button2;
            this.m_Pose = m_Pose;
            this.leftHand = leftHand;
            this.rightHand = rightHand;
        }
        #endregion
    }
}