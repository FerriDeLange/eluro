using System;
using System.Runtime.Serialization;

namespace Eluro.InputManagement
{
	[Serializable]
	internal class InputDeviceNotFoundException : Exception
	{
		private Type type;

		public InputDeviceNotFoundException()
		{
		}

		public InputDeviceNotFoundException(Type type, string message) : base(message) => this.type = type;

		public InputDeviceNotFoundException(string message) : base(message)
		{
		}

		public InputDeviceNotFoundException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected InputDeviceNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}