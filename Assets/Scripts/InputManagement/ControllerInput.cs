using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.InputSystem;

namespace Eluro.InputManagement
{
	/// <summary>
	/// The input manager for a standard game controller.
	/// </summary>
    public class ControllerInput : IInput
    {
		#region Members
		private Gamepad gamepad
		{
			get
			{
				if (Gamepad.current == null)
					throw new InputDeviceNotFoundException(typeof(Gamepad), "The gamepad can't be found.");
				return Gamepad.current;
			}
		}

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public float Horizontal => gamepad.leftStick.ReadValue().x;

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public float Vertical => gamepad.leftStick.ReadValue().y;

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public float Trigger
		{
			get
			{
				if (gamepad.leftTrigger.ReadValue() > Mathf.Epsilon)
					return gamepad.leftTrigger.ReadValue();
				else if (gamepad.rightTrigger.ReadValue() > Mathf.Epsilon)
					return gamepad.rightTrigger.ReadValue();
				else
					return 0;
			}
		}

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public float Grab => 0;

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public bool Button1 => gamepad.aButton.ReadValue() > Mathf.Epsilon;

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public bool Button2 => gamepad.aButton.ReadValue() > Mathf.Epsilon;
		#endregion
	}
}