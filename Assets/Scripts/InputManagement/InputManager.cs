using Eluro.Eluro;
using Eluro.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace Eluro.InputManagement
{
    public class InputManager : SingletonBehaviour<InputManager>, IInput
    {
		#region Enum
		public enum InputType
		{
			Controller,
			Joycon,
            VRControls,
			KeyboardAndMouse
		}
		#endregion
		#region Members
		[SerializeField]
		private InputType selectedInputType;

		[SerializeField]
		private PortListener portListener;

        [Header ("SteamVR Setting")]
        [SerializeField]
        private SteamVR_ActionSet primarySet;

        [SerializeField]
        public SteamVR_Action_Single m_PinchVector = null;

        [SerializeField]
        public SteamVR_Action_Single m_GrabVector = null;

        [SerializeField]
        public SteamVR_Action_Vector2 m_TrackVector = null;

        [SerializeField]
        public SteamVR_Action_Boolean m_Button1 = null;

        [SerializeField]
        public SteamVR_Action_Boolean m_Button2 = null;

        [SerializeField]
        private SteamVR_Behaviour_Pose m_Pose = null;

        [SerializeField]
        private Hand leftHand, rightHand;

        private Dictionary<InputType, IInput> inputTypeMapper;

		public float Horizontal => inputTypeMapper[selectedInputType].Horizontal;

		public float Vertical => inputTypeMapper[selectedInputType].Vertical;

		public float Trigger => inputTypeMapper[selectedInputType].Trigger;

        public float Grab => inputTypeMapper[selectedInputType].Grab;

		public bool Button1 => inputTypeMapper[selectedInputType].Button1;

		public bool Button2 => inputTypeMapper[selectedInputType].Button2;
		#endregion
		#region Unity Methods
		protected override void Awake()
        {
            base.Awake();
			inputTypeMapper = new Dictionary<InputType, IInput>
			{ 
				{ InputType.KeyboardAndMouse, new KeyboardAndMouseInput()                                                                                            },
				{ InputType.Controller,       new ControllerInput()                                                                                                  },
				{ InputType.Joycon,           new JoyconInput(portListener)                                                                                          },
                { InputType.VRControls,       new VRInput(primarySet, m_PinchVector, m_GrabVector, m_TrackVector, m_Button1, m_Button2, m_Pose, rightHand, leftHand) },
			};
        }
		#endregion
		#region Methods
		#endregion
	}
}