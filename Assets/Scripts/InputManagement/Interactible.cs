using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactible : MonoBehaviour
{
    [HideInInspector]
    public Hand m_activeHand = null;
}
