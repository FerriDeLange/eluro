using Eluro.Eluro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Eluro.InputManagement
{
	/// <summary>
	/// The input manager for the custom controller.
	/// </summary>
    public class JoyconInput : IInput
	{
		#region Members
		private PortListener portListener;

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public float Horizontal => -portListener.Input.x;

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public float Vertical => -portListener.Input.y;

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public float Trigger => Mouse.current.leftButton.ReadValue();// gamepad.leftTrigger.ReadValue() > Mathf.Epsilon || gamepad.rightTrigger.ReadValue() > Mathf.Epsilon;

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public float Grab => 0;

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public bool Button1 => false;// gamepad.aButton.ReadValue() > Mathf.Epsilon;

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public bool Button2 => false;// gamepad.aButton.ReadValue() > Mathf.Epsilon;
		#endregion
		#region Constructor
		public JoyconInput(PortListener portListener)
		{
			this.portListener = portListener;
		}
		#endregion
	}
}