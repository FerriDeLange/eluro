using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Eluro.InputManagement
{
	/// <summary>
	/// Defines behaviour for control input.
	/// </summary>
    public interface IInput
    {
		#region Methods
		/// <summary>
		/// Should return a value between -1 and 1. Defines the horizontal axis of the control.
		/// </summary>
		float Horizontal { get; }

		/// <summary>
		/// Should return a value between -1 and 1. Defines the vertical axis of the control.
		/// </summary>
		float Vertical { get; }

		/// <summary>
		/// Should be true when the button is being pressed.
		/// </summary>
		float Trigger { get; }

        /// <summary>
        /// Should be true when the Grab is hold.
        /// </summary>
        float Grab { get; }

		/// <summary>
		/// Should be true when the button is being pressed.
		/// </summary>
		bool Button1 { get; }

		/// <summary>
		/// Should be true when the button is being pressed.
		/// </summary>
		bool Button2 { get; }
		#endregion
	}
}