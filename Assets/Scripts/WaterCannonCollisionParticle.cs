using System;
using System.Collections.Generic;
using UnityEngine;

namespace Eluro.WaveSystem
{
	/// <summary>
	/// This class handles the particles during collision between water cannon and enemies.
	/// </summary>
    public class WaterCannonCollisionParticle : MonoBehaviour
    {
		#region Members
		/// <summary>
		/// The action to invoke when we hit an enemy.
		/// </summary>
		public event Action<Enemy> EnemyHit;
        #endregion
        #region Unity Methods

		/// <summary>
		/// This method checks for collision between particles and objects, and fires event if true.
		/// </summary>
		/// <param name="other">The GameObject collided with.</param>
        private void OnParticleCollision(GameObject other)
		{
			// Check if the GameObject we hit is an Enemy.
			if (other.TryGetComponent(out Enemy enemy))
			{
				// Invoke action.
				EnemyHit?.Invoke(enemy);
			}
		}
		#endregion
	}
}