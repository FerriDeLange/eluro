using Eluro.InputManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Eluro.Cannon
{
	/// <summary>
	/// The water cannon behaviour. This is the main manager to do everything water cannon related; shooting, aiming, pressure managing, etc.
	/// </summary>
	public class WaterCannon : MonoBehaviour
    {
		#region Members
		/// <summary>
		/// The instance of the CannonAimer.
		/// </summary>
		private CannonAimer aimer;

		/// <summary>
		/// The data for the cannon aimer.
		/// </summary>
		[SerializeField]
		private CannonAimerDataSO aimerData;

		/// <summary>
		/// The barrel transform of the water cannon.
		/// </summary>
		[SerializeField]
		private Transform barrelTransform;

		/// <summary>
		/// The shoot behaviour of the water cannon.
		/// </summary>
		[SerializeField]
		private WaterCannonShooter shooter;

		/// <summary>
		/// The shoot behaviour of the water cannon.
		/// </summary>
		public WaterCannonShooter Shooter => shooter;
		#endregion
		#region Unity Methods
		/// <summary>
		/// Creates the Cannon aimer instance.
		/// </summary>
		public void Start() => aimer = new CannonAimer(transform, barrelTransform, aimerData);

		/// <summary>
		/// Checks for input and acts on it.
		/// </summary>
		public void Update()
        {
			aimer.UpdateAngle();

			Vector2 direction = new Vector2(InputManager.Instance.Horizontal, InputManager.Instance.Vertical);
			if (!(direction.x > -0.05 && direction.x < 0.05) || !(direction.y > -0.05 && direction.y < 0.05))
				Aim(direction);

			try
			{
				shooter.SetValve(InputManager.Instance.Trigger);
			}
			catch (InputDeviceNotFoundException e)
			{
				Debug.LogException(e);
			}
		}

		/// <summary>
		/// Draws the view angles.
		/// </summary>
		private void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.yellow;
			Gizmos.DrawLine(transform.position, transform.position + new Vector3(Mathf.Cos(aimerData.Data.MaxYRotation * Mathf.Deg2Rad) * 5, 0, Mathf.Sin(aimerData.Data.MaxYRotation * Mathf.Deg2Rad)) * 5);
			Gizmos.DrawLine(transform.position, transform.position + new Vector3(Mathf.Cos(aimerData.Data.MinYRotation * Mathf.Deg2Rad) * 5, 0, Mathf.Sin(aimerData.Data.MinYRotation * Mathf.Deg2Rad)) * 5);
		}
		#endregion
		#region Methods
		/// <summary>
		/// Aims the water cannon.
		/// </summary>
		/// <param name="direction">The direction to aim in.</param>
		public void Aim(Vector2 direction) => aimer.Aim(direction);
		#endregion
	}
}