using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Eluro.Cannon
{
	/// <summary>
	/// Defines the aim behaviour for the cannon.
	/// </summary>
    public class CannonAimer
    {
		#region Members
		/// <summary>
		/// Reference to the transform of the cannon.
		/// </summary>
		private readonly Transform cannonTransform;

		/// <summary>
		/// Reference to the transform of the cannon barrel.
		/// </summary>
		private readonly Transform cannonBarrelTransform;

		/// <summary>
		/// Reference to the data of the cannon. This contains everything relevant for operating the aim behaviour.
		/// </summary>
		private readonly CannonAimerDataSO cannonAimerData;

		/// <summary>
		/// The current angular velocity of the cannon.
		/// </summary>
		private Vector3 angularVelocity;
		#endregion
		#region Constructor
		/// <summary>
		/// Creates a new cannon aimer instance.
		/// </summary>
		/// <param name="cannonTransform">Reference to the transform of the cannon.</param>
		/// <param name="cannonBarrelTransform">Reference to the transform of the cannon barrel.</param>
		/// <param name="cannonAimerData">Reference to the data of the cannon. This contains everything relevant for operating the aim behaviour.</param>
		public CannonAimer(Transform cannonTransform, Transform cannonBarrelTransform, CannonAimerDataSO cannonAimerData)
        {
			this.cannonTransform = cannonTransform;
			this.cannonBarrelTransform = cannonBarrelTransform;
			this.cannonAimerData = cannonAimerData;

			angularVelocity = Vector2.zero;
		}
        #endregion
        #region Methods
		/// <summary>
		/// Aims the cannon in a certain direction on the Y and Z axis.
		/// </summary>
		/// <param name="direction">Teh direction to aim in. Vector2.x = Y axis, Vector2.y = Z axis.</param>
		public void Aim(Vector2 direction)
		{
			//direction.Normalize();
			angularVelocity += new Vector3(0, direction.x, direction.y) * cannonAimerData.Data.AccelerateBy * Time.deltaTime;
			if(angularVelocity.magnitude > cannonAimerData.Data.MaxSpeed)
				angularVelocity = angularVelocity.normalized * cannonAimerData.Data.MaxSpeed;
		}

		/// <summary>
		/// Updates the behaviour of the cannon. Should be called in a Monobehaviour update method.
		/// </summary>
		public void UpdateAngle()
		{
			angularVelocity *= cannonAimerData.Data.Drag;
			Vector3 rotation = new Vector3();
			rotation.y = cannonTransform.eulerAngles.y + angularVelocity.y;
			rotation.z = cannonBarrelTransform.eulerAngles.x + angularVelocity.z;
			if (rotation.z > 180)
				rotation.z -= 360;
			if (rotation.y > 360)
				rotation.y -= 360;
			if (rotation.y < 0)
				rotation.y += 360;
			rotation.z = Mathf.Clamp(rotation.z, cannonAimerData.Data.MinZRotation, cannonAimerData.Data.MaxZRotation);
			rotation.y = Mathf.Clamp(rotation.y, cannonAimerData.Data.MinYRotation, cannonAimerData.Data.MaxYRotation);
			cannonTransform.localEulerAngles = new Vector3(0, rotation.y, 0);
			cannonBarrelTransform.localEulerAngles = new Vector3(rotation.z, 0, 0);
		}
        #endregion
    }
}