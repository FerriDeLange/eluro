using Eluro.ShopManagement;
using Eluro.WaveSystem;
using UnityEngine;

namespace Eluro.Cannon
{
	/// <summary>
	/// The shoot behaviour for the water cannon.
	/// </summary>
	public class WaterCannonShooter : MonoBehaviour
	{
		/// <summary>
		/// The primary water beam particle.
		/// </summary>
		[SerializeField]
		private ParticleSystem waterbeamPrimaryParticle;

		/// <summary>
		/// The secondary water beam particle.
		/// </summary>
		[SerializeField]
		private ParticleSystem waterbeamSecondaryParticle;

		[SerializeField]
		private ParticleSystem waterbeamCollisionParticle;

		/// <summary>
		/// The primary water muzzle particle.
		/// </summary>
		[SerializeField]
		private ParticleSystem watermuzzlePrimaryParticle;

		/// <summary>
		/// The secondary water muzzle particle.
		/// </summary>
		[SerializeField]
		private ParticleSystem watermuzzleSecondaryParticle;

		/// <summary>
		/// How much the valve is opened, between 0 and 1.
		/// </summary>
		private float valveAmount = 0.0f;

		public float Pressure => pressure;
		public float MaxPressure => maxPressure;
		
		/// <summary>
		/// The strength to emit particles.
		/// </summary>
		private float emissionStrength = 75;

		/// <summary>
		/// The current amount of pressure.
		/// </summary>
		[SerializeField]
		private float pressure;
		
		/// <summary>
		/// How much the maximum amount of pressure is.
		/// </summary>
		[SerializeField]
		private float maxPressure;

		/// <summary>
		/// By how much the pressure will go down.
		/// </summary>
		[SerializeField]
		private float pressureUsage;

		/// <summary>
		/// By how much the pressure will be regenarated.
		/// </summary>
		[SerializeField]
		private float pressureRegenerationAmount;

		[SerializeField]
		private WaterCannonCollisionParticle collisionParticle;

		/// <summary>
		/// Whether the pressure is blocked from being used.
		/// </summary>
		private bool pressureBlocked;

		/// <summary>
		/// Set the pressure equal to the maximum amount.
		/// </summary>
		private void Start() => pressure = maxPressure;

		private void OnEnable()
		{
			collisionParticle.EnemyHit += OnEnemyHit;
		}
		private void OnDisable()
		{
			collisionParticle.EnemyHit -= OnEnemyHit;
		}

		private string WaterHitEvent = "event:/WaterSplash/Water splash";
		[SerializeField]
		private FMODUnity.StudioEventEmitter emitter;
		private bool testBool = false;


		/// <summary>
		/// The amount of damage the water cannon does.
		/// </summary>
		public int Damage
		{
			get
			{
				float inital = 20;
				DamageBoost boost = ShopInfo<DamageBoost>.Get().Product;
				return Mathf.RoundToInt(inital * (boost.Purchased ? boost.DamageMultiplier : 1));
			}
		}

		/// <summary>
		/// Handles the pressure system; use of pressure, regenaration, blockage.
		/// </summary>
		private void Update()
		{
			var primaryModule = waterbeamPrimaryParticle.emission;
			var secondaryModule = waterbeamSecondaryParticle.emission;
			var collisionModule = waterbeamCollisionParticle.emission;

			if (valveAmount == 0 || pressureBlocked)
				pressure += pressureRegenerationAmount * Time.deltaTime;
			pressure = Mathf.Clamp(pressure, 0, maxPressure);
			if (pressure / maxPressure > 0.3f)
			{
				pressureBlocked = false;
				
			}

			if (pressure > 0 && !pressureBlocked)
			{

				

				primaryModule.rateOverTime   = valveAmount * emissionStrength;
				collisionModule.rateOverTime = valveAmount * emissionStrength;
				secondaryModule.rateOverTime = primaryModule.rateOverTime.constant * 0.866666f;

				if (valveAmount > 0)
				{
					watermuzzlePrimaryParticle  .Play();
					watermuzzleSecondaryParticle.Play();
					waterbeamCollisionParticle  .Play();

					if (testBool == false)
					{
						emitter.Play();
						testBool = true;
					}

				}
				else
				{
					watermuzzlePrimaryParticle  .Stop(false, ParticleSystemStopBehavior.StopEmitting);
					watermuzzleSecondaryParticle.Stop(false, ParticleSystemStopBehavior.StopEmitting);
					waterbeamCollisionParticle  .Stop(false, ParticleSystemStopBehavior.StopEmitting);
					if (testBool == true)
					{
						emitter.Stop();
						testBool = false;
					}
				}

				pressure -= valveAmount * (pressureUsage * Time.deltaTime);




			}
			else
			{
				

				
				pressureBlocked = true;

				primaryModule.rateOverTime = 0;
				collisionModule.rateOverTime = 0;
				secondaryModule.rateOverTime = 0;
				watermuzzlePrimaryParticle  .Stop(false, ParticleSystemStopBehavior.StopEmitting);
				watermuzzleSecondaryParticle.Stop(false, ParticleSystemStopBehavior.StopEmitting);
				waterbeamCollisionParticle  .Stop(false, ParticleSystemStopBehavior.StopEmitting);
				
			}
		}

		/// <summary>
		/// Sets the valve to a certain amount.
		/// </summary>
		/// <param name="amount">Amount to set the valve to.</param>
		public void SetValve(float amount) => valveAmount = Mathf.Clamp01(amount);

		private void OnEnemyHit(Enemy enemy)
		{
			FMODUnity.RuntimeManager.PlayOneShot(WaterHitEvent, transform.position);
			enemy.TakeDamage(Damage);
		}
	}
}