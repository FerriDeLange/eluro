using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Eluro.Cannon
{
	/// <summary>
	/// Scriptable object to contain the cannon aimer data.
	/// </summary>
	[CreateAssetMenu(fileName = "CannonAimerDataSO", menuName = "CannonAimerDataSO")]
	public class CannonAimerDataSO : ScriptableObject
	{
		#region Members
		/// <summary>
		/// The cannon aimer data.
		/// </summary>
		public CannonAimerData Data => data;

		/// <summary>
		/// The cannon aimer data.
		/// </summary>
		[SerializeField]
		private CannonAimerData data;
		#endregion
	}
}