using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Eluro.Cannon
{
	/// <summary>
	/// The data relevant for aiming with the cannon.
	/// </summary>
	[Serializable]
    public struct CannonAimerData
    {
		#region Members
		/// <summary>
		/// The max rotation on the Y axis.
		/// </summary>
		public float MaxYRotation => maxYRotation;

		/// <summary>
		/// The min rotation on the Y axis.
		/// </summary>
		public float MinYRotation => minYRotation;

		/// <summary>
		/// The max rotation on the Z axis.
		/// </summary>
		public float MaxZRotation => maxZRotation;

		/// <summary>
		/// The min rotation on the Z axis.
		/// </summary>
		public float MinZRotation => minZRotation;

		/// <summary>
		/// The max speed the cannon aim.
		/// </summary>
		public float MaxSpeed => maxSpeed;

		/// <summary>
		/// How much to accelerate by.
		/// </summary>
		public float AccelerateBy => accelerateBy;

		/// <summary>
		/// The drag of the aim.
		/// </summary>
		public float Drag => drag;

		/// <summary>
		/// The max rotation on the Y axis.
		/// </summary>
		[SerializeField, Range(0, 360)]
		private float maxYRotation;

		/// <summary>
		/// The min rotation on the Y axis.
		/// </summary>
		[SerializeField, Range(0, 360)]
		private float minYRotation;

		/// <summary>
		/// The max rotation on the Z axis.
		/// </summary>
		[SerializeField, Range(-180, 180)]
		private float maxZRotation;

		/// <summary>
		/// The min rotation on the Z axis.
		/// </summary>
		[SerializeField, Range(-180, 180)]
		private float minZRotation;

		/// <summary>
		/// The max speed the cannon aim.
		/// </summary>
		[SerializeField]
		private float maxSpeed;

		/// <summary>
		/// How much to accelerate by.
		/// </summary>
		[SerializeField]
		private float accelerateBy;

		/// <summary>
		/// The drag of the aim.
		/// </summary>
		[SerializeField]
		private float drag;
		#endregion
		#region Constructor
		/// <summary>
		/// Creates a new cannon aimer data container.
		/// </summary>
		/// <param name="maxYRotation">The max rotation on the Y axis.</param>
		/// <param name="minYRotation">The min rotation on the Y axis.</param>
		/// <param name="maxZRotation">The max rotation on the Z axis.</param>
		/// <param name="minZRotation">The min rotation on the Z axis.</param>
		/// <param name="maxSpeed">The max speed the cannon aim.</param>
		/// <param name="accelerateBy">How much to accelerate by.</param>
		/// <param name="drag">The drag of the aim.</param>
		public CannonAimerData(float maxYRotation, float minYRotation, float maxZRotation, float minZRotation, float maxSpeed, float accelerateBy, float drag)
		{
			this.maxYRotation = maxYRotation;
			this.minYRotation = minYRotation;
			this.maxZRotation = maxZRotation;
			this.minZRotation = minZRotation;
			this.maxSpeed = maxSpeed;
			this.accelerateBy = accelerateBy;
			this.drag = drag;
		}
		#endregion
	}
}