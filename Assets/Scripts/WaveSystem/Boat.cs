using System;
using UnityEngine;

namespace Eluro.WaveSystem
{
    /// <summary>
    /// This class holds variables for the player/main boat.
    /// The boat has a reference to its maximum/current health, and can take damage.
    /// </summary>
    public class Boat : MonoBehaviour, IHittable
    {
        #region Members
        /// <summary>
        /// The maximum health possible.
        /// </summary>
        [SerializeField, Tooltip("Enter health for this unit.")]
        private int maxHealth;

		[SerializeField]
		private float speed = 2;

        /// <summary>
        /// The current value of health.
        /// </summary>
        private int currentHealth;

        /// <summary>
        /// The maximum health possible.
        /// </summary>
        public int MaxHealth => maxHealth;

		public bool IsDead => currentHealth <= 0;

		public event Action Died;
		private bool dead;

        /// <summary>
        /// The current value of health.
        /// </summary>
        public int CurrentHealth => currentHealth;

        /// <summary>
        /// The event to invoke when taking damage to announce current health.
        /// </summary>
        public event Action<int> HealthChanged;

        private string DamageEvent = "event:/BoatImpact/BoatImpact";
        private string WaveLost = "event:/Waves/Waves_Lost";

        #endregion
        #region Unity Methods
        private void OnEnable()
        {
            // Set current health equal to max health when the game starts.
            currentHealth = maxHealth;

            // Announce current health.
            HealthChanged?.Invoke(currentHealth);
            
        }

		private void Update()
		{

			transform.localPosition += Vector3.right * Time.deltaTime * speed;

		}
		#endregion
		#region Methods
		/// <summary>
		/// This method subtracts damage from current health and announces current health.
		/// </summary>
		/// <param name="damage"></param>
		public void TakeDamage(int damage)
        {
            // Subtract damage from current health.
            currentHealth -= damage;
            FMODUnity.RuntimeManager.PlayOneShot(DamageEvent, transform.position);

			if (currentHealth < 0)
            {
                Debug.Log("Boat died");
				//FMODUnity.RuntimeManager.PlayOneShot(WaveLost, transform.position);
				if (!dead)
					Died?.Invoke();
				dead = true;
            }
				

            // Invoke current health.
            HealthChanged?.Invoke(currentHealth);
        }
        #endregion
    }
}