namespace Eluro.WaveSystem
{
    /// <summary>
    /// This interface makes sure that classes contain these methods when this interface is implemented.
    /// </summary>
    public interface IHittable
    {
        #region Methods
        /// <summary>
        /// This method makes the gameobject take damage.
        /// </summary>
        /// <param name="amount">Amount of damage to take.</param>
        public void TakeDamage(int amount);
        #endregion
    }
}