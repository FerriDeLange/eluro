using UnityEngine;

namespace Eluro.WaveSystem
{
    /// <summary>
    /// This class is used to create a circular spawn region using the objects transform to scale it.
    /// </summary>
    public class EllipseSpawnRegion : SpawnRegion
    {
        #region Unity Methods
        /// <summary>
        /// This method is used to visually show the region in the editor.
        /// </summary>
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(transform.position, transform.localScale.x);
        }
        #endregion
        #region Methods
        /// <summary>
        /// This method returns a random Vector3 within the region.
        /// </summary>
        /// <returns>Returns a Random Vector3 within the region.</returns>
        public override Vector3 GetRandomLocation()
        {
            Vector3 randomPosition = Random.insideUnitSphere * transform.localScale.x + transform.position;
            randomPosition.y = transform.position.y;
            return randomPosition;
        }
        #endregion
    }
}