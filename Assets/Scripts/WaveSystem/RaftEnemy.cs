using UnityEngine;

namespace Eluro.WaveSystem
{
	public class RaftEnemy : Enemy
	{
		[SerializeField]
		private ParticleSystem deathExplosion;

		protected override void Die()
		{
			base.Die();
			deathExplosion.Play(true);
		}
	}
}