using UnityEngine;

namespace Eluro.WaveSystem
{
    /// <summary>
    /// This class makes sure that objects that derive from it contain these methods.
    /// </summary>
    public abstract class SpawnRegion : MonoBehaviour
    {
        #region Methods
        /// <summary>
        /// This method returns a random Vector3 from within the region.
        /// </summary>
        /// <returns>Returns a random Vector3 from within the region.</returns>
        public abstract Vector3 GetRandomLocation();
        #endregion
    }
}