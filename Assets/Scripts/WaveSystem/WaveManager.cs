using Eluro.InputManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Eluro.WaveSystem
{
    /// <summary>
    /// This class manages the starting and ending of waves, and the intervals between waves.
    /// This class invokes events when starting waves/intervals.
    /// </summary>
    public class WaveManager : MonoBehaviour
    {
        #region Members
        /// <summary>
        /// A reference to the spawn manager, used to subscribe to events.
        /// </summary>
        [SerializeField, Tooltip("Drag SpawnManager here.")]
        private SpawnManager spawnManager;

        /// <summary>
        /// A list filled with all the wave scriptable objects.
        /// </summary>
        [SerializeField, Tooltip("Drag waves here.")]
        private List<Wave> waveList;

        /// <summary>
        /// The main queue for waves to start. Filled with the waves from waveList.
        /// </summary>
        private Queue<Wave> remainingWaves;

        /// <summary>
        /// A reference to keep track of the wave currently in progress.
        /// </summary>
        private Wave currentWave;

        /// <summary>
        /// A reference to the current wave state.
        /// </summary>
        private WaveState waveState;

        /// <summary>
        /// A reference to the current wave's index. Useful for UI elements.
        /// </summary>
        private int currentWaveIndex;

        /// <summary>
        /// A reference to keep track of the wave currently in progress.
        /// </summary>
        public Wave CurrentWave => currentWave;

        /// <summary>
        /// A reference to the current wave's index. Useful for UI elements.
        /// </summary>
        public int CurrentWaveIndex => currentWaveIndex;

        /// <summary>
        /// A reference to the current wave state.
        /// </summary>
        public WaveState WaveState => waveState;

        /// <summary>
        /// A timer used by the interval.
        /// </summary>
        private float timer;

		[SerializeField]
		private Boat boat;

        /// <summary>
        /// This value determines how long an interval lasts.
        /// </summary>
        [SerializeField, Tooltip("Enter how long an interval should last in seconds.")]
        private float intervalDurationSeconds;

        /// <summary>
        /// The event to invoke when starting a wave.
        /// </summary>
        public event Action<Wave> StartedWave;

        /// <summary>
        /// The event to invoke when starting a wave. Has the variable of the current wave index.
        /// </summary>
        public event Action<int> StartedWaveIndex;

        /// <summary>
        /// The event to invoke when starting an interval.
        /// </summary>
        public event Action StartedInterval;

        private string MusicEvent = "event:/BackgroundMusic/Piraten_Muziek_schets_1";
        

        #endregion
        #region Unity Methods
        /// <summary>
        /// This method is used to subscribe to events,
        /// and start the main coroutine.
        /// </summary>
        public void OnEnable()
        {
            FMODUnity.RuntimeManager.MuteAllEvents(false);
            FMODUnity.RuntimeManager.PlayOneShot(MusicEvent, transform.position);

            // Setup waveState to wait for user input.
            waveState = WaveState.WAIT_FOR_INPUT;

            // Start the main coroutine.
            StartCoroutine(StartWaves());

            // Subscribe to event.
            spawnManager.AllEnemiesDead += EndWave;
        }

        /// <summary>
        /// Method used to unsubscribe from events.
        /// </summary>
        private void OnDisable()
        {
            spawnManager.AllEnemiesDead -= EndWave;
            FMODUnity.RuntimeManager.MuteAllEvents(true);
        }

        /// <summary>
        /// Update method is used to decrease timer during intervals.
        /// When timer reaches zero, end the interval.
        /// </summary>
        public void Update()
        {
            if (waveState == WaveState.INTERVAL)
            {

				if(boat.IsDead)
				{
					waveState = WaveState.WAIT_FOR_INPUT;
					remainingWaves = new Queue<Wave>(waveList);
				}

                timer -= Time.deltaTime;

                if (timer <= 0)
                {
                    EndInterval();
                }
            }
        }
        #endregion
        #region Methods
		public void Interact()
		{
			if(waveState == WaveState.WAIT_FOR_INPUT)
			{
				ContinueWaves();
			}
		}

        /// <summary>
        /// The main coroutine.
        /// While there are still waves left in the waveList,
        /// start the next wave and wait until the wave is over, as well as the interval.
        /// </summary>
        private IEnumerator StartWaves()
        {
            // Make a new queue, filled with all the waves in waveList.
            remainingWaves = new Queue<Wave>(waveList);

            while (remainingWaves.Count > 0)
            {
                // Wait for user input.
                yield return new WaitUntil(() => waveState == WaveState.CONTINUE_SPAWNING);
                StartWave();
            }
        }

        /// <summary>
        /// This method will dequeue the current wave,
        /// remove it from the list,
        /// increase the wave index,
        /// update the current state and invoke events.
        /// </summary>
        private void StartWave()
        {
            // Set current wave and dequeue it from the queue.
            currentWave = remainingWaves.Dequeue();

            // Update index.
            currentWaveIndex++;

            // Update state.
            waveState = WaveState.SPAWNING;

            // Fire events.
            StartedWave?.Invoke(currentWave);
            StartedWaveIndex?.Invoke(currentWaveIndex);
        }

        /// <summary>
        /// This method is invoked by SpawnManager event.
        /// Will start the interval method.
        /// </summary>
        private void EndWave()
        {
            StartInterval();
        }

        /// <summary>
        /// This method resets the interval timer,
        /// update the wave state and invoke events.
        /// </summary>
        private void StartInterval()
        {
            // Reset timer.
            timer = intervalDurationSeconds;

            // Update state.
            waveState = WaveState.INTERVAL;

            // Invoke event.
            StartedInterval?.Invoke();
        }

        /// <summary>
        /// When timer reaches 0, this method ends the interval.
        /// </summary>
        private void EndInterval()
        {
            waveState = WaveState.WAIT_FOR_INPUT;
        }

        /// <summary>
        /// This method modifies the current wave state. The next wave will start when method is called.
        /// </summary>
        private void ContinueWaves()
        {
            waveState = WaveState.CONTINUE_SPAWNING;
        }
        #endregion
    }
}