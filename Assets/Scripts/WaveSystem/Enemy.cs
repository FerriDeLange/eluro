using Eluro.Cannon;
using Eluro.ShopManagement;
using System;
using System.Linq;
using UnityEngine;
using WaterPhysics;

namespace Eluro.WaveSystem
{
	/// <summary>
	/// This class contains variables and methods that determine an enemy's behaviour.
	/// </summary>
	[RequireComponent(typeof(Rigidbody))]
    public class Enemy : MonoBehaviour, IHittable
    {
        #region Members
        /// <summary>
        /// A reference to the target of this unit.
        /// </summary>
        private Transform currentTarget;

		/// <summary>
		/// A reference to the rigidbody.
		/// </summary>
		private new Rigidbody rigidbody;

		/// <summary>
		/// The amount of damage this unit does when it attacks its target.
		/// </summary>
		[SerializeField, Tooltip("Enter how many damage this unit does.")]
		private int attackDamage;

		/// <summary>
		/// The health this unit starts out with.
		/// </summary>
		[SerializeField, Tooltip("Set health points for this unit.")]
		private int maxHealth;

		/// <summary>
		/// The amount of health points this unit currently has.
		/// </summary>
		private int currentHealth;

		/// <summary>
		/// This variable determines how fast the unit moves.
		/// </summary>
		[SerializeField, Tooltip("Enter movement speed.")]
		private float movementSpeed;

		/// <summary>
		/// The amount of money the enemies reward when they're defeated.
		/// </summary>
		[SerializeField]
		private int moneyReward;

		/// <summary>
		/// The particle system that emits particles when hit by the water cannon.
		/// </summary>
		[SerializeField]
		private ParticleSystem onHitParticleSystem;

		/// <summary>
		/// The particle system that emits particles when hit by the water cannon.
		/// </summary>
		public ParticleSystem OnHitParticleSystem => onHitParticleSystem;

		/// <summary>
		/// The amount of money the enemies reward when they're defeated.
		/// </summary>
		public int MoneyReward => moneyReward;

		/// <summary>
		/// A reference to the target of this unit.
		/// </summary>
		public Transform CurrentTarget => currentTarget;

		/// <summary>
		/// A reference to the rigidbody.
		/// </summary>
		public Rigidbody Rigidbody => rigidbody;

		/// <summary>
		/// The amount of damage this unit does when it attacks its target.
		/// </summary>
		public int AttackDamage => attackDamage;

		/// <summary>
		/// The health this unit starts out with.
		/// </summary>
		public int MaxHealth => maxHealth;

		/// <summary>
		/// The amount of health points this unit currently has.
		/// </summary>
		public int CurrentHealth => currentHealth;

		/// <summary>
		/// This variable determines how fast the unit moves.
		/// </summary>
		public float MovementSpeed => movementSpeed;

		/// <summary>
		/// This action is invoked when the unit dies.
		/// </summary>
		public event Action EnemyDeath;

		/// <summary>
		/// This action is invoked when taking damage.
		/// </summary>
		public event Action<int> HealthChanged;

		private float? wasHitTime;

		private string AttackEvent = "event:/CatSounds/Angry/Angry 1";
		private string DyingEvent = "event:/CatSounds/Dying/Dying 5";



		#endregion
		#region Unity Methods
		/// <summary>
		/// Method is used to set variables.
		/// </summary>
		private void OnEnable()
		{
			// Set reference to the rigidbody.
			rigidbody = GetComponent<Rigidbody>();
			//onHitParticleSystems = onHitParticleGameObject.GetComponentsInChildren<ParticleSystem>();
			currentHealth = maxHealth;
		}

		public void FixedUpdate()
		{
			// Move towards the target.
			Move();

			if (wasHitTime != null && Time.time - wasHitTime > 0.1f)
			{
				wasHitTime = null;
			}
		}

		/// <summary>
		/// This method checks if the unit hits the target, in this case the boat.
		/// </summary>
		/// <param name="collision">The collision that happens when colliding with another gameobject.</param>
		private void OnTriggerEnter(Collider collision)
		{
			// Keep a reference to the object we collided with.
			var components = collision.gameObject.GetComponents<MonoBehaviour>();
			if (components.Length > 0)
			{
				IHittable target = (IHittable)components.First((MonoBehaviour m) => m is IHittable);

				// If the target is hittable, attack it.
				if (target != null && target is Boat)
                {
					Attack(target);
					FMODUnity.RuntimeManager.PlayOneShot(AttackEvent, transform.position);
				}
					

			}
		}
		#endregion
		#region Methods
		/// <summary>
		/// This method deals damage when it touches the target, called when it collides with the target.
		/// </summary>
		/// <param name="target">The target to attack.</param>
		private void Attack(IHittable target)
		{
			// Deal damage to the target.
			target.TakeDamage(attackDamage);

			Die();
		}

		/// <summary>
		/// This method moves the unit towards its target.
		/// </summary>
		private void Move()
		{
			// If no target is assigned, return.
			if (currentTarget == null) return;

			// Look at the target.
			transform.LookAt(currentTarget);
			transform.forward = new Vector3(transform.forward.x, 0, transform.forward.z);

			// Move the unit forward.
			rigidbody.AddForce(transform.forward * movementSpeed / 100f, ForceMode.VelocityChange);
		}

		/// <summary>
		/// This method sets the target.
		/// Called when instantiated in SpawnManager.
		/// </summary>
		/// <param name="tf">The transform of the target to set.</param>
		public void SetTarget(Transform tf)
		{
			currentTarget = tf;
		}

		/// <summary>
		/// This method is called when taking damage, and announces current health.
		/// </summary>
		/// <param name="amount">The amount of damage to take.</param>
		public void TakeDamage(int amount)
		{
			if (wasHitTime == null && currentHealth > 0)
			{
				wasHitTime = Time.time;
				currentHealth -= amount;

				// Checking if particle system isn't currently playing, to prevent overlap.
				if (onHitParticleSystem.isStopped)
                {
					// Play on hit particles.
					onHitParticleSystem.Play(true);
                }

				if (currentHealth <= 0)
				{
					Die();
					FMODUnity.RuntimeManager.PlayOneShot(DyingEvent, transform.position);
					Player.Instance.AddMoney(moneyReward);
				}

				HealthChanged?.Invoke(currentHealth);
			}
		}

		/// <summary>
		/// This method handles the death of the enemy.
		/// Also invokes the death event.
		/// </summary>
		protected virtual void Die()
		{
			// Invoke the death of the enemy.
			EnemyDeath?.Invoke();

			// Destroy the enemy after 3 seconds.
			Destroy(gameObject, 3f);

            //currentTarget = null;

            // Get a reference to all floaters.
            Floater[] floaters = transform.GetComponentsInChildren<Floater>();


			// Lower each floater's displacement amount to let the enemy sink.
			foreach (Floater floater in floaters)
            {
				// Sink.
				LeanTween.value(floater.displacementAmount, 0.05f, 0.75f)
					.setEase(LeanTweenType.easeInQuad)
					.setOnUpdate((float v) => floater.displacementAmount = v);

				// Slow down after delay.
				LeanTween.value(floater.waterDrag, 15f, 2f)
					.setOnUpdate((float v) => floater.waterDrag = v)
					.setDelay(0.6f);
			}
		}
		#endregion
	}
}