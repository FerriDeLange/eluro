namespace Eluro.WaveSystem
{
    /// <summary>
    /// This enum defines Several states of the wave.
    /// Used to handle the flow of a wave.
    /// </summary>
    public enum WaveState
    {
        SPAWNING,
        INTERVAL,
        WAIT_FOR_INPUT,
        CONTINUE_SPAWNING,
    }
}