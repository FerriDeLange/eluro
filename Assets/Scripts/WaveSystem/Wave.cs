using System.Collections.Generic;
using UnityEngine;
using System;

namespace Eluro.WaveSystem
{
	/// <summary>
	/// This scriptable object is used to create a wave in the editor.
	/// A wave consists of enemies that will be spawned at certain thresholds(progress).
	/// </summary>
	[CreateAssetMenu(fileName = "Wave", menuName = "Wave")]
	public class Wave : ScriptableObject
	{
		#region Members
		/// <summary>
		/// The duration of the entire wave.
		/// </summary>
		public float durationSeconds;

		/// <summary>
		/// A list that holds all EnemyProgress pairs.
		/// </summary>
		public List<EnemyProgressPair> enemyProgressPairs;
		#endregion

		/// <summary>
		/// This class is used to create a pair.
		/// The enemy will be spawned when the progress is reached.
		/// The count is used how many entities will be spawned when progress is reached.
		/// </summary>
		[Serializable]
		public class EnemyProgressPair
        {
			public Enemy enemy;
			public float progress;
			public int count = 1;
		}
	}
}