using UnityEngine;

namespace Eluro.WaveSystem
{
    /// <summary>
    /// This class is used to create a rectangular spawn region using the objects transform to scale it.
    /// </summary>
    public class RectangleSpawnRegion : SpawnRegion
    {
        #region Unity Methods
        /// <summary>
        /// This method is used to visually show the region in the editor.
        /// </summary>
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(transform.position, transform.localScale);
        }
        #endregion
        #region Methods
        /// <summary>
        /// This method returns a random Vector3 within the region.
        /// </summary>
        /// <returns>Returns a Random Vector3 within the region.</returns>
        public override Vector3 GetRandomLocation() 
        {
            float x = Random.Range(-transform.localScale.x / 2, transform.localScale.x / 2);
            float y = Random.Range(-transform.localScale.y / 2, transform.localScale.y / 2);
            float z = Random.Range(-transform.localScale.z / 2, transform.localScale.z / 2);
            return new Vector3(x, y, z) + transform.position;
        }
        #endregion
    }
}