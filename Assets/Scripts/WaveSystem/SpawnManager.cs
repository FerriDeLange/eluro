using Eluro.Tiles;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Eluro.WaveSystem
{
	/// <summary>
	/// This class manages the spawning of enemies using a queue.
	/// This class holds references to all the spawn regions, used to give a random position to spawned enemies.
	/// This class subscribes to events from WaveManager.
	/// </summary>
	public class SpawnManager : MonoBehaviour
	{
		#region Members
		/// <summary>
		/// This list is used to keep track of all the different spawn regions, and used by method to return a random location.
		/// </summary>
		[SerializeField, Tooltip("Drag spawn locations here.")]
		private List<SpawnRegion> spawnRegions;

		/// <summary>
		/// A reference to the wave manager for events.
		/// </summary>
		[SerializeField, Tooltip("Drag WaveManager here.")]
		private WaveManager waveManager;

		/// <summary>
		/// A reference to a target, used to set target for spawned enemies.
		/// </summary>
		[SerializeField, Tooltip("Drag desired target here.")]
		private GameObject target;

		[SerializeField]
		private IslandPlacer tileManager;

		/// <summary>
		/// This list is used to keep track of enemies that are still alive in the current wave.
		/// If this list is empty, the wave is over.
		/// </summary>
		private List<Enemy> enemiesAlive;

		/// <summary>
		/// The main queue for spawning enemies.
		/// Will spawn enemies depending on their progress during a wave.
		/// </summary>
		private Queue<Wave.EnemyProgressPair> enemies;

		/// <summary>
		/// A reference of the current wave.
		/// Used to grab data.
		/// </summary>
		private Wave currentWave;

		/// <summary>
		/// waveTime is used to progress a wave. Enemies will be spawned when waveTime hits certain thresholds.
		/// </summary>
		private float waveTime;

		/// <summary>
		/// The event that will be fired when there are no enemies left in this wave.
		/// WaveManager will listen to this.
		/// </summary>
		public event Action AllEnemiesDead;

		public ReadOnlyCollection<Enemy> AliveEnemies => enemiesAlive.AsReadOnly();

		private IEnumerable<SpawnRegion> validSpawnRegionsQuery;
		#endregion
		#region Unity Methods
		/// <summary>
		/// This method will be used to create the new list for enemies alive.
		/// </summary>
		private void Awake()
		{
			validSpawnRegionsQuery = from region in spawnRegions
									 where Vector3.Distance(region.transform.position, target.transform.position) < 100
									 select region;
			enemiesAlive = new List<Enemy>();
		}

		/// <summary>
		/// This method is used to subscribe from events.
		/// </summary>
		public void OnEnable()
		{
			waveManager.StartedWave += Setup;
			tileManager.TileSpawned += OnTileSpawned;
			tileManager.TileDestroyed += OnTileDestroyed;
		}

		/// <summary>
		/// This method is used to unsubscribe from events.
		/// </summary>
		private void OnDisable()
		{
			waveManager.StartedWave -= Setup;
			tileManager.TileSpawned += OnTileSpawned;
			tileManager.TileDestroyed += OnTileDestroyed;
		}

		/// <summary>
		/// In this method, wavetime is increased to progress a wave.
		/// </summary>
		public void Update()
		{
			waveTime += Time.deltaTime;
		}
		#endregion
		#region Methods
		/// <summary>
		/// The Setup method will be called when a wave starts.
		/// This method will start the coroutine for spawning enemies during the wave. 
		/// </summary>
		/// <param name="wave">The wave's data will be used to create the routine for spawning enemies.</param>
		private void Setup(Wave wave)
		{
			// Save a reference to the current wave.
			currentWave = wave;

			// Reset the wave time.
			waveTime = 0;

			// Clear the list at the start of the wave.
			enemiesAlive.Clear();

			// Start the coroutine for spawning enemies.
			StartCoroutine(StartSpawning(currentWave.enemyProgressPairs));
		}
		/// <summary>
		/// This coroutine will wait until waveTime matches an enemies threshold.
		/// When it does, it will spawn the enemy,
		/// and repeat this for every enemy that should be spawned in the current wave.
		/// </summary>
		/// <param name="pairs">The enemy progress pair to spawn.</param>
		private IEnumerator StartSpawning(List<Wave.EnemyProgressPair> pairs)
		{
			// Make a new queue with the pairs within the current wave.
			enemies = new Queue<Wave.EnemyProgressPair>(pairs);

			// While there are still enemies left in the queue,..
			while (enemies.Count > 0)
			{
				// Calculate time until next enemy should spawn.
				float remainingTime = enemies.Peek().progress * currentWave.durationSeconds;

				// Wait until the wavetime reaches the threshold of the next enemy-to-spawn's threshold.
				yield return new WaitUntil(() => waveTime >= remainingTime);

				// If the threshold is met, spawn the enemy and dequeue the current.
				StartCoroutine(SpawnEnemy(enemies.Dequeue()));
			}
		}

		/// <summary>
		/// This method will instantiate an enemy,
		/// set it's position to a random position in a random spawn region,
		/// add it to the enemiesAlive list to keep track of it,
		/// set it's target,
		/// and subscribe to it's death event.
		/// </summary>
		/// <param name="pair">The enemy progress pair.</param>
		private IEnumerator SpawnEnemy(Wave.EnemyProgressPair pair)
		{
			for (int i = 0; i < pair.count; i++)
			{
				// Spawn the enemy.
				Enemy enemy = Instantiate(pair.enemy);

				// Give the enemy a random position.
				enemy.transform.position = GetRandomSpawnLocation();

				// Add the enemy to the list.
				enemiesAlive.Add(enemy);

				// Set the target for the enemy.
				enemy.SetTarget(target.transform);

				// Create action to unsubscribe from enemy upon death.
				Action EnemyDeath = null;
				EnemyDeath = delegate () { RemoveEnemyFromList(enemy, EnemyDeath); };

				// Subscribe to the enemy death event.
				enemy.EnemyDeath += EnemyDeath;

				yield return new WaitForSeconds(Random.Range(0, 0.1f));
			}
		}

		/// <summary>
		/// This method will remove an enemy from the list that keeps track of its existance.
		/// When the list is empty, invoke an event.
		/// </summary>
		/// <param name="enemy">The enemy to remove from the list.</param>
		private void RemoveEnemyFromList(Enemy enemy, Action action)
		{
			// Unsubscribe from event.
			enemy.EnemyDeath -= action;

			// Remove enemy from list.
			enemiesAlive.Remove(enemy);
			if (enemiesAlive.Count == 0)
				AllEnemiesDead?.Invoke();
		}

		/// <summary>
		/// This method returns a random Vector3 from within a random spawn region.
		/// </summary>
		/// <returns>Returns a random Vector3 from within a random spawn region.</returns>
		private Vector3 GetRandomSpawnLocation()
		{
			var validRegions = validSpawnRegionsQuery.ToArray();
			Vector3 randomSpawnLocation = validRegions.ToArray()[UnityEngine.Random.Range(0, validRegions.Length)].GetRandomLocation();
			return randomSpawnLocation;
		}

		private void OnTileSpawned(Tile tile) => spawnRegions.AddRange(tile.SpawnRegions);
		private void OnTileDestroyed(Tile tile)
		{
			for (int i = 0; i < tile.SpawnRegions.Count; i++)
			{
				spawnRegions.Remove(tile.SpawnRegions[i]);
			}
		}
		#endregion
	}
}