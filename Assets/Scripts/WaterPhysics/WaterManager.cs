using UnityEngine;

namespace WaterPhysics
{
    /// <summary>
    /// This class creates a psuedo water wave, used by boats to create buoyancy.
    /// </summary>
    public class WaterManager : MonoBehaviour
    {
        #region Members
        /// <summary>
        /// The singleton instance.
        /// </summary>
        public static WaterManager instance;

        /// <summary>
        /// Length of the water plane.
        /// </summary>
        public float length = 2f;

        /// <summary>
        /// Speed variable used to control (psuedo) water animation speed.
        /// </summary>
        public float speed = 1f;

        /// <summary>
        /// Variable used to offset the waves.
        /// </summary>
        private float offset = 0;

        /// <summary>
        /// Height of the wave.
        /// </summary>
        public float amplitude = 1f;

        #endregion
        #region Unity Methods
        /// <summary>
        /// This method defines the singleton instance.
        /// </summary>
        public void Awake()
        {
            if (instance == null)
            {
                instance = this;
            } else if (instance != this) 
            {
                Destroy(this);
            }

        }

        /// <summary>
        /// The update method animates the psuedo water.
        /// </summary>
        public void Update()
        {
            offset += Time.deltaTime * speed;
        }
        #endregion
        #region Methods
        /// <summary>
        /// This method calculates the height of the wave at given position.
        /// </summary>
        /// <param name="xPosition">The position to check.</param>
        /// <returns>Returns the height of the wave at given position.</returns>
        public float GetWaveHeight(float xPosition)
        {
            return amplitude * Mathf.Sin(xPosition / length + offset);
        }
        #endregion
    }
}