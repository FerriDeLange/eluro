using UnityEngine;

namespace WaterPhysics
{
    /// <summary>
    /// This class manages how gravity applies to attached game object, depending on current height versus height of the water.
    /// </summary>
    public class Floater : MonoBehaviour
    {
        #region Members
        /// <summary>
        /// The rigidbody of the parent game object.
        /// </summary>
        public Rigidbody rigidBody;

        /// <summary>
        /// The depth before submerged float, used to determine when an object is considered submerged.
        /// </summary>
        public float depthBeforeSubmerged = 1f;

        /// <summary>
        /// The amount of displacement added to the rigidbody.
        /// </summary>
        public float displacementAmount = 3f;

        /// <summary>
        /// The number of objects used to calculate gravity.
        /// </summary>
        public int floaterCount = 1;

        /// <summary>
        /// The amount of drag used to add force to velocity.
        /// </summary>
        public float waterDrag = 0.99f;

        /// <summary>
        /// The amount of angular drag used to add torque to velocity.
        /// </summary>
        public float waterAngularDrag = 0.5f;
        #endregion
        #region Unity Methods

        /// <summary>
        /// This method calculates the how much gravity will be applied to the rigidbody, and at what position.
        /// This method relies on WaterManager to return the height of the wave at given position.
        /// </summary>
        public void FixedUpdate()
        {
            // Add force to the rigidbody at this floater position.
            rigidBody.AddForceAtPosition(Physics.gravity / floaterCount, transform.position, ForceMode.Acceleration);

            // Get the waveheight from WaterManager.
            float waveHeight = WaterManager.instance.GetWaveHeight(transform.position.x);

            // If this floater y position is lower than the waveheight..
            if (transform.position.y < waveHeight)
            {
                // Calculate displacement multiplier.
                float displacementMultiplier = Mathf.Clamp01((waveHeight - transform.position.y) / depthBeforeSubmerged) * displacementAmount;

                // Add force to the rigidbody at this floater position using acceleration.
                rigidBody.AddForceAtPosition(new Vector3(0f, Mathf.Abs(Physics.gravity.y) * displacementMultiplier, 0f), transform.position, ForceMode.Acceleration);

                // Add force to the rigidbody calculated by waterdrag using velocity.
                rigidBody.AddForce(displacementMultiplier * -rigidBody.velocity * waterDrag * Time.fixedDeltaTime, ForceMode.VelocityChange);

                // Add torque to the rigidbody calculated by water angular drag using velocity.
                rigidBody.AddTorque(displacementMultiplier * -rigidBody.angularVelocity * waterAngularDrag * Time.fixedDeltaTime, ForceMode.VelocityChange);
            }
        }
        #endregion
    }
}