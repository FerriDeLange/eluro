using Eluro.WaveSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Eluro.UI
{
	/// <summary>
	/// The user inteface behaviour for the health of the boat on the desk.
	/// </summary>
    public class HealthUI : MonoBehaviour
    {
		#region Members
		/// <summary>
		/// The target of this UI script.
		/// </summary>
		[SerializeField]
		private Boat boat;

		[SerializeField]
		private HorizontalLayoutGroup healthBarsParent;

		private Image[] healthBars;
		#endregion
		#region Unity Methods
		private void Start()
		{
			healthBars = healthBarsParent.GetComponentsInChildren<Image>();
		}

		/// <summary>
		/// Update the health progress based on the current and max health of the boat.
		/// </summary>
		public void Update()
        {
			for (int i = 0; i < healthBars.Length; i++)
			{
				float normalizedHealth = boat.CurrentHealth / (float)boat.MaxHealth;
				float normalizedIndex  =                  i / (float)healthBars.Length;
				Color c = healthBars[i].color;
				c.a = Mathf.Lerp(c.a, normalizedHealth - normalizedIndex, Time.deltaTime * 5);
				healthBars[i].color = c;
			}
        }
        #endregion
        #region Methods
        #endregion
    }
}