using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using Eluro.WaveSystem;
using UnityEngine.InputSystem;
using Eluro.Eluro;
using UnityEngine.SceneManagement;

namespace Eluro.UI
{
    public class InteractionUI : MonoBehaviour
    {
		#region Members
		[SerializeField]
		private TMP_Text label;
		[SerializeField]
		private PhysicsButton button;

		[SerializeField]
		private WaveManager waveManager;

		[SerializeField]
		private CanvasGroup group;

		[SerializeField]
		private Boat boat;

		private bool firstTime = true;
        #endregion
        #region Unity Methods
        public void OnEnable()
        {
			label.text = "Continue";
			button.OnPressed += OnButtonPressed;
			boat.Died += OnDeath;
        }

        public void OnDisable()
		{
			button.OnPressed -= OnButtonPressed;
			boat.Died -= OnDeath;
		}


		private void Update()
		{
			if (Keyboard.current.kKey.wasPressedThisFrame)
			{
				OnButtonPressed();
			}
		}
		#endregion
		#region Methods
		private void OnDeath()
		{
			label.text = "Restart";
		}

		private void OnButtonPressed()
		{
			if (boat.IsDead)
				SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

			if(firstTime)
			{
				firstTime = false;
				label.text = "Start";
				LeanTween.alphaCanvas(group, 0, 1f).setEaseInOutSine();
			}
			else
			{
				label.text = string.Empty;
				waveManager.Interact();
			}
		}
		#endregion
	}
}