using Eluro.WaveSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Eluro.UI
{
	/// <summary>
	/// The UI for the radar on the desk.
	/// </summary>
	public class RadarUI : MonoBehaviour
	{
		#region Members
		/// <summary>
		/// Reference to the spawn manager, for finding the enemy data.
		/// </summary>
		[SerializeField]
		private SpawnManager spawnManager;

		/// <summary>
		/// The prefab for the bleeps on the screen that represent enemy positions.
		/// </summary>
		[SerializeField]
		private Image bleepPrefab;

		/// <summary>
		/// All the active bleeps.
		/// </summary>
		private List<Image> bleeps;

		/// <summary>
		/// By how much the radar is zoomed.
		/// </summary>
		[SerializeField]
		private float zoomStrength = 1;

		/// <summary>
		/// The image that represents the turning green wave effect.
		/// </summary>
		[SerializeField]
		private Image radarEffect;

		/// <summary>
		/// The speed of the radar effect.
		/// </summary>
		[SerializeField]
		private float radarEffectSpeed = 1;

		/// <summary>
		/// The parent of where the bleeps are spawned in.
		/// </summary>
		[SerializeField]
		private RectTransform bleepParent;
		#endregion
		#region Unity Methods
		/// <summary>
		/// Initialize the script.
		/// </summary>
		public void Start()
		{
			bleeps = new List<Image>();
		}

		/// <summary>
		/// Updates the positions and colours of the bleeps on the radar.
		/// </summary>
		public void Update()
		{
			EqualizeList();

			for (int i = 0; i < bleeps.Count; i++)
			{
				Vector3 enemyPosition = spawnManager.AliveEnemies[i].transform.position;
				bleeps[i].transform.localPosition = new Vector3(enemyPosition.x, enemyPosition.z, 0) * zoomStrength;
				bleeps[i].color = GetBleepColor(bleeps[i].rectTransform.anchoredPosition);
			}

			radarEffect.transform.eulerAngles -= Vector3.forward * radarEffectSpeed;
			if (radarEffect.transform.eulerAngles.z < 360)
				radarEffect.transform.eulerAngles += new Vector3(0, 0, 360);
		}

		#endregion
		#region Methods
		/// <summary>
		/// Balances the active amount of bleeps with the amount of enemies that are currently in game.
		/// </summary>
		private void EqualizeList()
		{
			int delta = spawnManager.AliveEnemies.Count - bleeps.Count;
			if (delta > 0)
			{
				int toAdd = delta;
				for (int i = 0; i < toAdd; i++)
				{
					Image bleep = Instantiate(bleepPrefab, bleepParent, false);
					bleeps.Add(bleep);
				}
			}
			else if (delta < 0)
			{
				int toRemove = -delta;
				for (int i = bleeps.Count - 1; i >= bleeps.Count - toRemove; i--)
				{
					Destroy(bleeps[i].gameObject);
				}
				bleeps.RemoveRange(bleeps.Count - toRemove, toRemove);
			}
		}

		/// <summary>
		/// Retrieves the colour of a bleep based on his position.
		/// </summary>
		/// <param name="position">The position of the bleep</param>
		/// <returns>The correct colour.</returns>
		private Color GetBleepColor(Vector2 position)
		{
			float angle = Mathf.Atan2(position.y, position.x) * Mathf.Rad2Deg;

			float angleNormalized = angle / 360f;
			float angleEffectNormalized = 1 - (radarEffect.transform.localEulerAngles.z / 360f);
			float alpha = Mathf.Repeat(angleNormalized + angleEffectNormalized, 1);
			alpha = 1 - alpha;
			return new Color(1, 1, 1, alpha);
		}
		#endregion
	}
}