using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Eluro.UI
{
    public class WeightButton : MonoBehaviour
    {
		#region Members
		public event Action OnPress;

		[SerializeField]
		private Collider expected;
		#endregion
		#region Unity Methods
		private void OnCollisionEnter(Collision collision)
		{
			if (collision.collider == expected)
				OnPress?.Invoke();
		}
		#endregion
		#region Methods
		#endregion
	}
}