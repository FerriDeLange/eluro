using Eluro.Cannon;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Eluro.UI
{
	/// <summary>
	/// Displays the current pressure on the water tank.
	/// </summary>
    public class WaterPressureUI : MonoBehaviour
    {
		#region Members
		/// <summary>
		/// Reference to the shooter object that contains the pressure data.
		/// </summary>
		[SerializeField]
		private WaterCannonShooter waterCannon;

		/// <summary>
		/// The meshrenderer with the water shader.
		/// </summary>
		[SerializeField]
		private MeshRenderer waterContent;

		/// <summary>
		/// The speed of the wobble.
		/// </summary>
		[SerializeField]
		private float WobbleSpeed = 1f;

		/// <summary>
		/// The strength of the wobble.
		/// </summary>
		[SerializeField]
		private float Strength = 1f;

		/// <summary>
		/// Amount of wobble on the x axis.
		/// </summary>
		private float wobbleAmountX;

		/// <summary>
		/// Amount of wobble on the z axis.
		/// </summary>
		private float wobbleAmountZ;

		/// <summary>
		/// The amount by how much the water is filled.
		/// </summary>
		private float fillAmount;
		#endregion
		#region Unity Methods
		/// <summary>
		/// Update the shader with the correct data based on the pressure.
		/// </summary>
		private void Update()
		{
			waterContent.sharedMaterial.SetVector("_Pos", transform.position);
			fillAmount = Mathf.Lerp(fillAmount, 1 - waterCannon.Pressure / waterCannon.MaxPressure, Time.deltaTime * 3);
			waterContent.sharedMaterial.SetFloat("_FillAmount", fillAmount * 1.7f - 0.3f);
			Wobble();
		}
		#endregion
		#region Methods
		/// <summary>
		/// Determines the wobble for the shader.
		/// </summary>
		private void Wobble()
		{
			// make a sine wave of the decreasing wobble
			float pulse = 2 * Mathf.PI * WobbleSpeed;
			wobbleAmountX = Mathf.Sin(pulse * Time.time) * Strength;
			wobbleAmountZ = Mathf.Sin(pulse * Time.time) * Strength;

			// send it to the shader
			waterContent.material.SetFloat("_WobbleX", wobbleAmountX);
			waterContent.material.SetFloat("_WobbleZ", wobbleAmountZ);
		}
		#endregion
	}
}