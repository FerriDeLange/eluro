﻿using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Eluro
{
	[RequireComponent(typeof(Image))]
	public class RoundingEffect : BaseMeshEffect
	{
		#region Enum
		/// <summary>
		/// The different modes this effect uses to round sprites
		/// </summary>
		public enum RoundingMode
		{
			/// <summary>
			/// This is the standard rounding and just creates rounded corners
			/// </summary>
			Fill,

			/// <summary>
			/// This does the same as fill, but also empties out the center to create a border effect
			/// </summary>
			Border
		}
		#endregion
		#region Fields
		#region Const
		/// <summary>
		/// The maximum percentage the Border can go
		/// </summary>
		private const float MAX_FACTOR_BORDER = 0.25f;

		public const string RELATIVE_DIRECTORY = "Shaders";
		public const string FILL_MATERIAL_FILE_NAME = "UIRound.mat";
		public const string BORDER_MATERIAL_FILE_NAME = "UIRoundBorder.mat";
		public const string FILL_SHADER_FILE_NAME = "RoundedCorners.shader";
		public const string BORDER_SHADER_FILE_NAME = "RoundedCornersBorder.shader";
		#endregion
		#region Inspector
		/// <summary>
		/// The mode we're using to round this sprite with
		/// </summary>
		[SerializeField]
		[Tooltip("The mode we're using to round this object with")]
		private RoundingMode mode = RoundingMode.Fill;

		/// <summary>
		/// Whether the effect automatically full rounds out the sprite
		/// </summary>
		[SerializeField]
		[Tooltip("Whether the effect automatically fully rounds out the sprite")]
		private bool autoRounding;

		/// <summary>
		/// The normalized amount we're rounding out the object with (0 = none, 1 = full)
		/// </summary>
		[SerializeField]
		[Tooltip("The normalized amount we're rounding out the object with (0 = none, 1 = full)")]
		[Range(0, 1)]
		public float amount;

		/// <summary>
		/// The normalized amount from the edge we're filling in the sprite with (0 = none, 1 = full)
		/// </summary>
		[SerializeField]
		[Tooltip("The normalized amount from the edge we're filling in the sprite with (0 = none, 1 = full)")]
		[Range(0, 1)]
		private float borderAmount;
		#endregion
		#region Private
		/// <summary>
		/// Image component this effect works on
		/// </summary>
		private Image image;

		/// <summary>
		/// The extra data the UI shader can work with
		/// </summary>
		private Vector2 extraData;
		#region Static
		/// <summary>
		/// Reference to the material used for the fill effect
		/// </summary>
		private static Material fillMaterial;

		/// <summary>
		/// Reference to the material used for the border effect
		/// </summary>
		private static Material borderMaterial;
		#endregion
		#endregion
		#region Public Accessors
		/// <summary>
		/// Image component this effect works on
		/// </summary>
		public Image Image
		{
			get
			{
				if (image != null)
					return image;

				image = GetComponent<Image>();
				return image;
			}
		}

#if UNITY_EDITOR
		/// <summary>
		/// The mode we're using to round this sprite with
		/// </summary>
		public RoundingMode Mode
		{
			get => mode;
			set
			{
				Image.sprite = null;
				mode = value;
				switch (value)
				{
					case RoundingMode.Fill:
						Image.material = FillMaterial;
						break;
					case RoundingMode.Border:
						Image.material = BorderMaterial;
						break;
				}
			}
		}

		/// <summary>
		/// Whether the effect automatically full rounds out the sprite
		/// </summary>
		public bool AutoRounding => autoRounding;
		#region Static
		/// <summary>
		/// Reference to the material used for the fill effect
		/// </summary>
		public static Material FillMaterial
		{
			get
			{
				if (fillMaterial != null)
					return fillMaterial;
				string path = $"{RELATIVE_DIRECTORY}/{FILL_MATERIAL_FILE_NAME}";

				if (File.Exists($"{Application.dataPath}/{path}"))
					fillMaterial = AssetDatabase.LoadAssetAtPath($"Assets/{path}", typeof(Material)) as Material;
				return fillMaterial;
			}
		}

		/// <summary>
		/// Reference to the material used for the border effect
		/// </summary>
		public static Material BorderMaterial
		{
			get
			{
				if (borderMaterial != null)
					return borderMaterial;
				string path = $"{RELATIVE_DIRECTORY}/{BORDER_MATERIAL_FILE_NAME}";

				if (File.Exists($"{Application.dataPath}/{path}"))
					borderMaterial = AssetDatabase.LoadAssetAtPath($"Assets/{path}", typeof(Material)) as Material;
				return borderMaterial;
			}
		}
		#endregion
#endif
		#endregion
		#endregion
		#region Constructor   
		/// <summary>
		/// protected constructor so it doesn't get created with new
		/// </summary>
		protected RoundingEffect()
		{ }
		#endregion
		#region Methods
		/// <summary>
		/// Gets called by <see cref="BaseMeshEffect"/> when the mesh gets updated
		/// </summary>
		/// <param name="vh">The VertexHelper for getting all the vertices of a mesh</param>
		public override void ModifyMesh(VertexHelper vh)
		{
			RectTransform rectTransform = (RectTransform)transform;

			if (rectTransform.rect.height < rectTransform.rect.width)
				extraData.x = (autoRounding ? 1 : amount) * rectTransform.rect.height;
			else
				extraData.x = (autoRounding ? 1 : amount) * rectTransform.rect.width;

			extraData.y = rectTransform.rect.height * MAX_FACTOR_BORDER * borderAmount;

			UIVertex vert = new UIVertex();

			for (int i = 0; i < vh.currentVertCount; i++)
			{

				vh.PopulateUIVertex(ref vert, i);
				vert.uv1 = rectTransform.rect.size;
				vert.uv2 = extraData;
				vh.SetUIVertex(vert, i);
			}
		}

#if UNITY_EDITOR
		/// <summary>
		/// The reset calls make sure the default settings for the component are set
		/// </summary>
		public new void Reset()
		{
			Mode = RoundingMode.Fill;
			extraData.x = ((RectTransform)transform).rect.height;
		}
#endif
		#endregion
	}
}
