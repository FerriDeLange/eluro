using Eluro.WaveSystem;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

namespace Eluro.UI
{
	/// <summary>
	/// User interface behaviour for the enemies.
	/// </summary>
    public class EnemyUI : MonoBehaviour
    {
		#region Members
		/// <summary>
		/// The target of the UI.
		/// </summary>
		[SerializeField]
		private Enemy enemy;

		/// <summary>
		/// The text object where the money should be displayed.
		/// </summary>
		[SerializeField]
		private TMP_Text moneyText;

		/// <summary>
		/// The image that represents the health bar.
		/// </summary>
		[SerializeField]
		private Image healthBar;

		/// <summary>
		/// The canvas group used to fade health bar.
		/// </summary>
		[SerializeField]
		private CanvasGroup canvasGroup;

		/// <summary>
		/// Bool to keep track if canvas group is already fading out.
		/// </summary>
		private bool isFading;

		/// <summary>
		/// The amount of displayed health that's currently above the enemy.
		/// </summary>
		private float healthAmount;

		private static Camera Camera
		{
			get
			{
				if (camera == null)
					camera = FindObjectsOfType<Camera>().First((Camera cam) => cam.CompareTag("Cannon Camera"));

				return camera;
			}
		}

		private static Camera camera;
		#endregion
		#region Unity Methods
		private void OnEnable()
		{
			moneyText.text = $"${enemy.MoneyReward}";
		}

		public void Update()
        {
			transform.forward = Camera.transform.forward;
			healthAmount = Mathf.Lerp(healthAmount, enemy.CurrentHealth / (float)enemy.MaxHealth, Time.deltaTime * 7);
			healthBar.fillAmount = healthAmount;

			if (enemy.CurrentHealth <= 0 && !isFading)
				FadeCanvas();
        }
        #endregion
        #region Methods
		private void FadeCanvas()
        {
			isFading = true;
			LeanTween.alphaCanvas(canvasGroup, 0, 0.5f).setEase(LeanTweenType.easeInQuad);
		}
        #endregion
    }
}