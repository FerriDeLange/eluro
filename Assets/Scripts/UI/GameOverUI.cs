using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Eluro.WaveSystem;
using System;

namespace Eluro.UI
{
    public class GameOverUI : MonoBehaviour
    {
		#region Members
		[SerializeField]
		private CanvasGroup group;

		[SerializeField]
		private Boat boat;
		#endregion
		#region Unity Methods
		private void OnEnable()
		{
			boat.Died += OnDeath;
		}

		private void OnDisable()
		{
			boat.Died -= OnDeath;
		}

		#endregion
		#region Methods
		private void OnDeath()
		{
			LeanTween.alphaCanvas(group, 1, 1f).setEaseInOutSine();
		}
		#endregion
	}
}