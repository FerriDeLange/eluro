using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Eluro.ShopManagement;

namespace Eluro.UI
{
	/// <summary>
	/// The UI for the wallet of the user.
	/// </summary>
    public class WalletUI : MonoBehaviour
    {
        #region Members
		/// <summary>
		/// The text where the money is displayed.
		/// </summary>
		[SerializeField]
		private TMP_Text walletText;
        #endregion
        #region Unity Methods
		/// <summary>
		/// Sets the text of the players money on the Text object.
		/// </summary>
        public void Update()
        {
			walletText.text = $"${Player.Instance.Money}";
		}
        #endregion
        #region Methods
        #endregion
    }
}