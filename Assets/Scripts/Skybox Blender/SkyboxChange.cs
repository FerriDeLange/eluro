using Eluro.WaveSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Eluro.Skybox
{
	/// <summary>
	/// This class is responsible for changing the skyboxes from day to night during the interval between waves.
	/// </summary>
	public class SkyboxChange : MonoBehaviour
	{
		#region Members

		/// <summary>
		/// A reference to the wave manager, used to subscribe to events.
		/// </summary>
		[SerializeField, Tooltip("Drag WaveManager here.")]
		private WaveManager waveManager;

		[SerializeField]
		private Material skybox;

		[SerializeField]
		private Color dayFog, nightFog;

		private bool day = true;
		#endregion

		#region Unity Methods

		private void OnEnable()
		{
			RenderSettings.skybox = skybox;

			skybox.SetFloat("_Blend", day ? 0 : 1);
			waveManager.StartedInterval += CycleSkybox;
		}

		private void OnDisable()
		{
			waveManager.StartedInterval -= CycleSkybox;
		}
		#endregion

		#region Methods
		/// <summary>
		/// Responsible for changing blending the skyboxes from day to night.
		/// </summary>
		private void CycleSkybox()
		{
			LeanTween.value(day ? 0 : 1, day ? 1 : 0, 2)
				.setOnUpdate((float val) =>
				{
					RenderSettings.fogColor = Color.Lerp(dayFog, nightFog, val);
					skybox.SetFloat("_Blend", val);
				})
				.setOnComplete(() => day = !day);
		}
		#endregion
	}


}


