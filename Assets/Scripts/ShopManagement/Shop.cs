using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Eluro.ShopManagement
{
	/// <summary>
	/// Contains an interface for purchasing power ups.
	/// </summary>
	public class Shop : MonoBehaviour
	{
		#region Members
		/// <summary>
		/// Gets aclled when a power up has been purchased.
		/// </summary>
		public event Action<PowerUpTypes> Purchased;

		/// <summary>
		/// All the available power ups.
		/// </summary>
		[SerializeField]
		private PowerUp[] powerUps;
		#endregion
		#region Unity Methods
		#endregion
		#region Methods
		/// <summary>
		/// Purchase a power up of type <typeparamref name="T"/> for the given customer and shop info.
		/// </summary>
		/// <typeparam name="T">The power up to buy.</typeparam>
		/// <param name="customer">The customer to buy it for.</param>
		/// <param name="shopInfo">The purchase info for the item.</param>
		public void Purchase<T>(ICustomer customer, ShopInfo<T> shopInfo) where T : PowerUp
		{
			if (customer.TryBuy(shopInfo.Price))
			{
				customer.OnPurchase(shopInfo);
				Purchased?.Invoke(shopInfo.Product.Type);
			}
		}
		#endregion
	}
}