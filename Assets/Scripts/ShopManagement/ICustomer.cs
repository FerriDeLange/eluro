namespace Eluro.ShopManagement
{
	/// <summary>
	/// Should be implemented by objects that want to be able to purchase Power Ups.
	/// </summary>
	public interface ICustomer
	{
		/// <summary>
		/// Tries to purchase with the given amount of money.
		/// </summary>
		/// <param name="moneyAmount">The cost of the power up.</param>
		/// <returns>Whether the purchase was successful</returns>
		bool TryBuy(int moneyAmount);

		/// <summary>
		/// Callback when the purchase was successful.
		/// </summary>
		/// <typeparam name="T">The purchased power up</typeparam>
		/// <param name="shopInfo">The info about the purchase.</param>
		void OnPurchase<T>(ShopInfo<T> shopInfo) where T : PowerUp;
	}
}