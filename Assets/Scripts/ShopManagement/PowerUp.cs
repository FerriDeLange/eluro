using UnityEngine;

namespace Eluro.ShopManagement
{
	/// <summary>
	/// Base class for power ups.
	/// </summary>
	public abstract class PowerUp : MonoBehaviour
	{
		/// <summary>
		/// Whether it has been purchased.
		/// </summary>
		public bool Purchased { get; private set; } = false;

		/// <summary>
		/// What type of power up this is.
		/// </summary>
		public abstract PowerUpTypes Type { get; }

		/// <summary>
		/// Reference to the shop.
		/// </summary>
		[SerializeField]
		private Shop shop;

		public void OnEnable() => shop.Purchased += OnPurchase;
		public void OnDisable() => shop.Purchased -= OnPurchase;

		/// <summary>
		/// Callback for when a purchase in the shop was made (for any type).
		/// </summary>
		/// <param name="type">The type of the purchase that was made</param>
		protected virtual void OnPurchase(PowerUpTypes type)
		{
			if (type == Type)
				Purchased = true;
		}
	}
}