namespace Eluro.ShopManagement
{
	/// <summary>
	/// The damage boost upgrade gives a multiplier on the damage of the cannon.
	/// </summary>
	public class DamageBoost : PowerUp
	{
		/// <summary>
		/// By how much to multiply the damage.
		/// </summary>
		public float DamageMultiplier => 1.5f;

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public override PowerUpTypes Type => PowerUpTypes.DamageBoost;
	}
}