using Eluro.Utility;
using UnityEngine;

namespace Eluro.ShopManagement
{
	/// <summary>
	/// Contains behaviour and data relevant to the player, in this case mostly shop based.
	/// </summary>
	public class Player : SingletonBehaviour<Player>, ICustomer
	{
		/// <summary>
		/// The amount of money of the player.
		/// </summary>
		public int Money { get; private set; } = 0;

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		/// <param name="moneyAmount"><inheritdoc/></param>
		/// <returns><inheritdoc/></returns>
		public bool TryBuy(int moneyAmount)
		{
			if(Money - moneyAmount > 0)
			{
				Money -= moneyAmount;
				return true;
			}

			return false;
		}

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		/// <typeparam name="T"><inheritdoc/></typeparam>
		/// <param name="shopInfo"><inheritdoc/></param>
		public void OnPurchase<T>(ShopInfo<T> shopInfo) where T : PowerUp
		{

		}

		/// <summary>
		/// Adds money to the players wallet.
		/// </summary>
		/// <param name="amount">The amount of money to add.</param>
		public void AddMoney(int amount)
		{
			Money += amount;
		}
	}
}