using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Eluro.ShopManagement
{
	/// <summary>
	/// Info used for in the shop for power ups.
	/// </summary>
	/// <typeparam name="T">The power up this is relevant for.</typeparam>
	public abstract class ShopInfo<T> : MonoBehaviour where T : PowerUp
	{
		/// <summary>
		/// All the shop infos that exist.
		/// </summary>
		private static List<ShopInfo<T>> shopInfo = new List<ShopInfo<T>>();

		private void OnEnable()
		{
			shopInfo.Add(this);
		}

		private void OnDisable()
		{
			shopInfo.Remove(this);
		}

		/// <summary>
		/// The product this shop info is linked to.
		/// </summary>
		public T Product => product;

		/// <summary>
		/// The price of this product.
		/// </summary>
		public int Price => price;

		/// <summary>
		/// The price of this product.
		/// </summary>
		[SerializeField]
		private int price;

		/// <summary>
		/// The product this shop info is linked to.
		/// </summary>
		[SerializeField]
		private T product;

		/// <summary>
		/// Retrieves a shop info based on the type param.
		/// </summary>
		/// <returns>The shop info based on the type.</returns>
		public static ShopInfo<T> Get()
		{
			for (int i = 0; i < shopInfo.Count; i++)
				if (shopInfo[i].Product.GetType() == typeof(T))
					return shopInfo[i];

			return null;
		}
	}
}